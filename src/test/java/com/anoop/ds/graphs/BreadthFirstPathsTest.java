package com.anoop.ds.graphs;

import java.io.File;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import com.anoop.In;

public class BreadthFirstPathsTest {

	private static Graph graph;
	private BreadthFirstPaths underTest;
	
	@BeforeClass
	public static void init() {
		graph = new Graph(new In(new File("//Users//ajaloree//git//algorithms//src//test//java//com//anoop//ds//graphs//tinyG.txt")));
	}
	
	@Test
	public void testPathToVertex0FromVertex6() {
		underTest = new BreadthFirstPaths(graph, 0);
		Assert.assertEquals("6->0",underTest.printPathFrom(6));
	}

	@Test
	public void testPathToVertex0FromVertex3() {
		underTest = new BreadthFirstPaths(graph, 0);
		Assert.assertEquals("3->5->0",underTest.printPathFrom(3));
	}

	@Test
	public void testPathToVertex6FromVertex2() {
		underTest = new BreadthFirstPaths(graph, 6);
		Assert.assertEquals("2->0->6",underTest.printPathFrom(2));
	}

	@Test
	public void testNoPathToVertex0FromVertex7() {
		underTest = new BreadthFirstPaths(graph, 0);
		Assert.assertEquals("No path exists.",underTest.printPathFrom(7));
	}

}