package com.anoop.ds.graphs;

import java.io.File;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import com.anoop.In;

public class GraphTest {

	private static Graph graph;
	
	@BeforeClass
	public static void init() {
		graph = new Graph(new In(new File("//Users//ajaloree//git//algorithms//src//test//java//com//anoop//ds//graphs//tinyG.txt")));
	}
	
	@Test
	public void testNumVertices() {
		Assert.assertEquals(new Integer(13), graph.getNumVertices());
	}

	@Test
	public void testNumEdges() {
		Assert.assertEquals(new Integer(13), graph.getNumEdges());
	}

	@Test
	public void testMaxDegree() {
		Assert.assertEquals(new Integer(4), graph.maxDegree());
	}

	@Test
	public void testNumSelfLoops() {
		Assert.assertEquals(new Integer(0), graph.numSelfLoops());
	}

}
