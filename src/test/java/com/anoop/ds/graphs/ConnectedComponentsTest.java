package com.anoop.ds.graphs;

import java.io.File;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.junit.Assert;
import org.junit.Test;

import com.anoop.In;

public class ConnectedComponentsTest {

	
	@Test
	public void testConnectedComponentsTinyG() {
		
		Graph graph = new Graph(new In(new File("//Users//ajaloree//git//algorithms//src//test//java//com//anoop//ds//graphs//tinyG.txt")));
		ConnectedComponents cc = new ConnectedComponents(graph);
		Assert.assertEquals(new Integer(3), cc.getCount());
		Map<Integer, Set<Integer>> components = new HashMap<>();
		for(Integer i = 0; i < graph.getNumEdges(); i++) {
			components.putIfAbsent(cc.id(i), new HashSet<>());
			components.get(cc.id(i)).add(i);
		}
		System.out.println("Components: "+components);
	}

}
