package com.anoop.ds.graphs;

import java.io.File;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import com.anoop.In;

public class DepthFirstSearchTest {

	private static Graph graph;
	private DepthFirstSearch underTest;
	
	@BeforeClass
	public static void init() {
		graph = new Graph(new In(new File("//Users//ajaloree//git//algorithms//src//test//java//com//anoop//ds//graphs//tinyG.txt")));
	}
	
	@Test
	public void testIsConnectionsAtNode0() {
		underTest = new DepthFirstSearch(graph, 0);
		
		Assert.assertTrue(underTest.isConnected(0));
		Assert.assertTrue(underTest.isConnected(1));
		Assert.assertTrue(underTest.isConnected(2));
		Assert.assertTrue(underTest.isConnected(3));
		Assert.assertTrue(underTest.isConnected(4));
		Assert.assertTrue(underTest.isConnected(5));
		Assert.assertTrue(underTest.isConnected(6));
		
		Assert.assertFalse(underTest.isConnected(7));
		Assert.assertFalse(underTest.isConnected(8));
		Assert.assertFalse(underTest.isConnected(9));
		Assert.assertFalse(underTest.isConnected(10));
		Assert.assertFalse(underTest.isConnected(11));
		Assert.assertFalse(underTest.isConnected(12));
	}

	@Test
	public void testIsConnectionsAtNode9() {
		underTest = new DepthFirstSearch(graph, 9);
		
		Assert.assertTrue(underTest.isConnected(9));
		Assert.assertTrue(underTest.isConnected(10));
		Assert.assertTrue(underTest.isConnected(11));
		Assert.assertTrue(underTest.isConnected(12));
		
		Assert.assertFalse(underTest.isConnected(0));
		Assert.assertFalse(underTest.isConnected(1));
		Assert.assertFalse(underTest.isConnected(2));
		Assert.assertFalse(underTest.isConnected(3));
		Assert.assertFalse(underTest.isConnected(4));
		Assert.assertFalse(underTest.isConnected(5));
		Assert.assertFalse(underTest.isConnected(6));
		Assert.assertFalse(underTest.isConnected(7));
		Assert.assertFalse(underTest.isConnected(8));
	}
}
