package com.anoop.ds.stack;


import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;


public class IntLinkedStackTest {
	
	private IntLinkedStack stack;
	
	@Before
	public void init() {
		stack = new IntLinkedStack();
	}

	@Test
	public void testPush() throws Exception {
		
		stack.push(1);
		stack.push(2);
		stack.push(3);
		
		assertEquals(3, stack.peek());
		assertEquals(3, stack.size());
		
		assertEquals(3, stack.pop());
		assertEquals(2, stack.size());

		assertEquals(2, stack.pop());
		assertEquals(1, stack.size());
		
		assertEquals(1, stack.pop());
		assertEquals(0, stack.size());
	}
	
	@Test(expected = RuntimeException.class)
	public void testPeekOnEmptyStackThrowsException() {
		stack.peek();
	}

	@Test(expected = RuntimeException.class)
	public void testPopOnEmptyStackThrowsException() {
		stack.pop();
	}

	@Test
	public void testPushPop() throws Exception {
		for(int i = 1; i <= 100; i++) {
			stack.push(i);
			assertEquals(i, stack.peek());
		}
		assertEquals(100, stack.size());
		for(int i = 100; i >= 1; i--) {
			assertEquals(i, stack.pop());
		}
		assertEquals(0, stack.size());

	}
}
