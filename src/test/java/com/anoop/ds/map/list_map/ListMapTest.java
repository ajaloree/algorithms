package com.anoop.ds.map.list_map;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class ListMapTest {
	
	private ListMap<String, String> underTest;
	
	@Before
	public void init() {
		underTest = new ListMap<>();
	}
	
	@Test
	public void testSetAndGet() {
		underTest.set("a", "apple");
		underTest.set("b", "ball");
		Assert.assertEquals(2, underTest.size());
		Assert.assertEquals("apple", underTest.get("a"));
		Assert.assertEquals("ball", underTest.get("b"));
		underTest.delete("a");
		underTest.delete("b");
		Assert.assertEquals(0, underTest.size());
	}

}
