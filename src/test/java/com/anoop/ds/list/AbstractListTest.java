package com.anoop.ds.list;

import static org.junit.Assert.*;
import org.junit.Test;

public abstract class AbstractListTest {

	private final Object VALUE_A = "A";
	private final Object VALUE_B = "B";
	private final Object VALUE_C = "C";
	
	protected abstract MyList createList();
	
	
	@Test
	public void testInsertIntoEmptyList() {
		MyList ls = createList();
		
		assertEquals(0, ls.size());
		assertTrue(ls.isEmpty());
		
		ls.insert(0, VALUE_A);
		
		assertEquals(1, ls.size());
		assertFalse(ls.isEmpty());
		assertEquals(VALUE_A, ls.get(0));
	}
	
	@Test
	public void testInsertBetweenList() {
		MyList ls = createList();
		
		ls.insert(0, VALUE_A);
		ls.insert(1, VALUE_B);
		ls.insert(1, VALUE_C);
		
		assertEquals(3, ls.size());

		assertEquals(VALUE_A, ls.get(0));
		assertEquals(VALUE_C, ls.get(1));
		assertEquals(VALUE_B, ls.get(2));
	}
	
	@Test
	public void testInsertBeforeFirstElement() {
		MyList ls = createList();
		
		ls.insert(0, VALUE_A);
		ls.insert(0, VALUE_B);
		
		assertEquals(2, ls.size());

		assertEquals(VALUE_B, ls.get(0));
		assertEquals(VALUE_A, ls.get(1));
	}

	@Test
	public void testInsertAfterLastElement() {
		MyList ls = createList();
		
		ls.insert(0, VALUE_A);
		ls.insert(1, VALUE_B);
		
		assertEquals(2, ls.size());

		assertEquals(VALUE_A, ls.get(0));
		assertEquals(VALUE_B, ls.get(1));
	}

	@Test(expected=Exception.class)
	public void testInsertOutOfBounds() {
		MyList ls = createList();
		ls.insert(1,VALUE_A);
	}
	
	@Test
	public void testAdd() {
		MyList ls = createList();
		
		ls.add(VALUE_A);
		ls.add(VALUE_B);
		ls.add(VALUE_C);
		
		assertEquals(3, ls.size());

		assertEquals(VALUE_A, ls.get(0));
		assertEquals(VALUE_B, ls.get(1));
		assertEquals(VALUE_C, ls.get(2));
	}

	@Test
	public void testSet() {
		MyList ls = createList();
		
		ls.insert(0, VALUE_A);
		Object ret = ls.set(0, VALUE_B);
		
		assertEquals(VALUE_A, ret);
		assertEquals(VALUE_B, ls.get(0));
	}
	
	@Test(expected=IndexOutOfBoundsException.class)
	public void testGetOutOfBoundsInEmptyList() {
		MyList ls = createList();
		ls.get(0);
	}
	
	@Test(expected=IndexOutOfBoundsException.class)
	public void testGetOutOfBoundsInNonEmptyList() {
		MyList ls = createList();
		ls.add(VALUE_A);
		ls.get(1);
	}
	
	@Test
	public void testDeleteOnlyElement() {
		MyList ls = createList();
		ls.add(VALUE_A);

		assertEquals(1, ls.size());
		assertEquals(VALUE_A, ls.get(0));
		
		Object ret = ls.delete(0);

		assertEquals(VALUE_A, ret);
		assertEquals(0, ls.size());
	}
	
	@Test
	public void testDeleteFirstElement() {
		MyList ls = createList();
		ls.add(VALUE_A);
		ls.add(VALUE_B);

		assertEquals(2, ls.size());
		
		Object ret = ls.delete(0);

		assertEquals(VALUE_A, ret);
		assertEquals(1, ls.size());
		assertEquals(VALUE_B, ls.get(0));
	}
	
	@Test
	public void testDeleteLastElement() {
		MyList ls = createList();
		ls.add(VALUE_A);
		ls.add(VALUE_B);

		assertEquals(2, ls.size());
		
		Object ret = ls.delete(1);

		assertEquals(VALUE_B, ret);
		assertEquals(1, ls.size());
		assertEquals(VALUE_A, ls.get(0));
	}
	
	@Test(expected=IndexOutOfBoundsException.class)
	public void testDeleteOutOfBounds() {
		MyList ls = createList();
		ls.add(VALUE_A);
		ls.delete(1);
	}
	
	@Test
	public void testDeleteByValue() {
		MyList ls = createList();
		ls.add(VALUE_A);
		ls.add(VALUE_B);
		ls.add(VALUE_C);

		assertEquals(3, ls.size());
		
		Object ret = ls.delete(1);

		assertEquals(VALUE_B, ret);
		assertEquals(2, ls.size());
		assertEquals(VALUE_A, ls.get(0));
		assertEquals(VALUE_C, ls.get(1));
	}
}