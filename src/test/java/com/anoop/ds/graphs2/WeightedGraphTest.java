package com.anoop.ds.graphs2;

import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;

public class WeightedGraphTest {

	static WeightedGraph<String> cityGraph;

	@BeforeClass
	public static void init() {

		cityGraph = new WeightedGraph<>(
				List.of("Seattle", "San Francisco", "Los Angeles", "Riverside", "Phoenix", 
						"Chicago", "Boston", "New York", "Atlanta", "Miami", 
						"Dallas", "Houston", "Detroit", "Philadelphia", "Washington"));
		
		cityGraph.addEdge("Seattle", "Chicago", 1737);
		cityGraph.addEdge("Seattle", "San Francisco", 678);
		cityGraph.addEdge("San Francisco", "Riverside", 386);
		cityGraph.addEdge("San Francisco", "Los Angeles", 348);
		cityGraph.addEdge("Los Angeles", "Riverside", 50);
		cityGraph.addEdge("Los Angeles", "Phoenix", 357);
		cityGraph.addEdge("Riverside", "Phoenix", 307);
		cityGraph.addEdge("Riverside", "Chicago", 1704);
		cityGraph.addEdge("Phoenix", "Dallas", 887);
		cityGraph.addEdge("Phoenix", "Houston", 1015);
		cityGraph.addEdge("Dallas", "Chicago", 805);
		cityGraph.addEdge("Dallas", "Atlanta", 721);
		cityGraph.addEdge("Dallas", "Houston", 225);
		cityGraph.addEdge("Houston", "Atlanta", 702);
		cityGraph.addEdge("Houston", "Miami", 968);
		cityGraph.addEdge("Atlanta", "Chicago", 588);
		cityGraph.addEdge("Atlanta", "Washington", 543);
		cityGraph.addEdge("Atlanta", "Miami", 604);
		cityGraph.addEdge("Miami", "Washington", 923);
		cityGraph.addEdge("Chicago", "Detroit", 238);
		cityGraph.addEdge("Detroit", "Boston", 613);
		cityGraph.addEdge("Detroit", "Washington", 396);
		cityGraph.addEdge("Detroit", "New York", 482);
		cityGraph.addEdge("Boston", "New York", 190);
		cityGraph.addEdge("New York", "Philadelphia", 81);
		cityGraph.addEdge("Philadelphia", "Washington", 123);
		System.out.println(cityGraph);
	}

	@Test
	public void testMinimumSpanningTree() throws Exception {
		
		cityGraph.printWeightedPath(cityGraph.mst(0));
		
		cityGraph.printWeightedPath(cityGraph.mst(5));

	}
}
