package com.anoop.ds.graphs2;

import java.util.List;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

public class UnweightedGraphTest {
	
	static UnweightedGraph<String> cityGraph;
	
	@BeforeClass
	public static void init() {

		cityGraph = new UnweightedGraph<>(
				List.of("Seattle", "San Francisco", "Los Angeles", "Riverside", "Phoenix", 
						"Chicago", "Boston", "New York", "Atlanta", "Miami", 
						"Dallas", "Houston", "Detroit", "Philadelphia", "Washington"));
		cityGraph.addEdge("Seattle", "Chicago");
		cityGraph.addEdge("Seattle", "San Francisco");
		cityGraph.addEdge("San Francisco", "Riverside");
		cityGraph.addEdge("San Francisco", "Los Angeles");
		cityGraph.addEdge("Los Angeles", "Riverside");
		cityGraph.addEdge("Los Angeles", "Phoenix");
		cityGraph.addEdge("Riverside", "Phoenix");
		cityGraph.addEdge("Riverside", "Chicago");
		cityGraph.addEdge("Phoenix", "Dallas");
		cityGraph.addEdge("Phoenix", "Houston");
		cityGraph.addEdge("Dallas", "Chicago");
		cityGraph.addEdge("Dallas", "Atlanta");
		cityGraph.addEdge("Dallas", "Houston");
		cityGraph.addEdge("Houston", "Atlanta");
		cityGraph.addEdge("Houston", "Miami");
		cityGraph.addEdge("Atlanta", "Chicago");
		cityGraph.addEdge("Atlanta", "Washington");
		cityGraph.addEdge("Atlanta", "Miami");
		cityGraph.addEdge("Miami", "Washington");
		cityGraph.addEdge("Chicago", "Detroit");
		cityGraph.addEdge("Detroit", "Boston");
		cityGraph.addEdge("Detroit", "Washington");
		cityGraph.addEdge("Detroit", "New York");
		cityGraph.addEdge("Boston", "New York");
		cityGraph.addEdge("New York", "Philadelphia");
		cityGraph.addEdge("Philadelphia", "Washington");
	}
	
	@Test
	public void testToString() {
		String graphStr = cityGraph.toString();
		System.out.println(graphStr);
		Assert.assertNotNull(graphStr);
	}
	
	@Test
	public void testBfsFromDallasToSeattle() {
		
		List<String> route = cityGraph.bfsPath("Dallas", v -> v.equalsIgnoreCase("Seattle"));
		
		System.out.println(route);
		
		Assert.assertEquals(3, route.size());
		Assert.assertEquals("Dallas", route.get(0));
		Assert.assertEquals("Chicago", route.get(1));
		Assert.assertEquals("Seattle", route.get(2));
	}

	@Test
	public void testShortestRouteBetweenNewYorkAndLosAngeles() {

		List<String> route = cityGraph.bfsPath("New York", v -> v.equalsIgnoreCase("Los Angeles"));
		
		System.out.println(route);
		
		Assert.assertEquals(5, route.size());
		Assert.assertEquals("New York", route.get(0));
		Assert.assertEquals("Detroit", route.get(1));
		Assert.assertEquals("Chicago", route.get(2));
		Assert.assertEquals("Riverside", route.get(3));
		Assert.assertEquals("Los Angeles", route.get(4));

	}

	@Test
	public void testShortestRouteBetweenBostonAndWashington() {
	
		List<String> route = cityGraph.bfsPath("Boston", v -> v.equalsIgnoreCase("Washington"));
		
		System.out.println(route);
		
		Assert.assertEquals(3, route.size());
		Assert.assertEquals("Boston", route.get(0));
		Assert.assertEquals("Detroit", route.get(1));
		Assert.assertEquals("Washington", route.get(2));
	}

	
	@Test
	public void testShortestRouteBetweenBostonAndHouston() {

		List<String> route = cityGraph.bfsPath("Boston", v -> v.equalsIgnoreCase("Houston"));
		
		System.out.println(route);
		
		Assert.assertEquals(5, route.size());
		Assert.assertEquals("Boston", route.get(0));
		Assert.assertEquals("Detroit", route.get(1));
		Assert.assertEquals("Chicago", route.get(2));
		Assert.assertEquals("Dallas", route.get(3));
		Assert.assertEquals("Houston", route.get(4));

	}

}
