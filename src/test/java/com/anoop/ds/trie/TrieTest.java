package com.anoop.ds.trie;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

public class TrieTest {

	@Test
	public void testInsertion() {
		
		Trie trie = new Trie();
		
		trie.insert("cat");
		trie.insert("cap");
		trie.insert("bat");
		
		System.out.println(trie);
		
		Assert.assertNotNull(trie.toString());
	}
	
	@Test
	public void testCollectAllWords() {
		
		Trie trie = new Trie();

		trie.insert("cat");
		trie.insert("cap");
		trie.insert("bat");

		List<String> ls = new ArrayList<>();
		trie.collectAllWords(null, "", ls);
		
		System.out.println(ls);
		
		Assert.assertEquals(3, ls.size());
	}
	
	@Test
	public void testAutocomplete() {
		
		Trie trie = new Trie();

		trie.insert("cat");
		trie.insert("cap");
		trie.insert("caterpillar");
		trie.insert("caper");
		trie.insert("bat");
		trie.insert("battery");
		trie.insert("batter");
		
		List<String> ret1 = trie.autocomplete("ca");
		System.out.println(ret1);
		Assert.assertEquals(4, ret1.size());
		Assert.assertTrue(ret1.contains("cat"));
		Assert.assertTrue(ret1.contains("cap"));
		Assert.assertTrue(ret1.contains("caper"));
		Assert.assertTrue(ret1.contains("caterpillar"));

		List<String> ret2 = trie.autocomplete("bat");
		System.out.println(ret2);
		Assert.assertEquals(3, ret2.size());
		Assert.assertTrue(ret2.contains("bat"));
		Assert.assertTrue(ret2.contains("battery"));
		Assert.assertTrue(ret2.contains("batter"));
	}
}
