package com.anoop.ds.trees;

import java.util.List;

import org.junit.Test;

import org.junit.Assert;

public class ListOfDepthsTest {
	
	ListOfDepths underTest = new ListOfDepths();
	
	@Test
	public void testListChildren() {
		
		//Create nodes
		BinaryTreeNode _50 = new BinaryTreeNode(50, "50");
		BinaryTreeNode _40 = new BinaryTreeNode(40, "40");
		BinaryTreeNode _60 = new BinaryTreeNode(60, "60");
		BinaryTreeNode _30 = new BinaryTreeNode(30, "30");
		BinaryTreeNode _45 = new BinaryTreeNode(45, "45");
		BinaryTreeNode _55 = new BinaryTreeNode(55, "55");
		BinaryTreeNode _70 = new BinaryTreeNode(70, "70");
		BinaryTreeNode _20 = new BinaryTreeNode(20, "20");
		BinaryTreeNode _35 = new BinaryTreeNode(35, "35");
		BinaryTreeNode _43 = new BinaryTreeNode(43, "43");
		BinaryTreeNode _49 = new BinaryTreeNode(49, "49");
		BinaryTreeNode _51 = new BinaryTreeNode(51, "51");
		BinaryTreeNode _59 = new BinaryTreeNode(59, "59");
		BinaryTreeNode _69 = new BinaryTreeNode(69, "69");
		BinaryTreeNode _80 = new BinaryTreeNode(80, "80");

		//Link nodes
		_30.left = _20;
		_30.right = _35;
		_45.left = _43;
		_45.right = _49;
		_55.left = _51;
		_55.right = _59;
		_70.left = _69;
		_70.right = _80;
		
		_40.left = _30;
		_40.right = _45;
		_60.left = _55;
		_60.right = _70;
		
		_50.left = _40;
		_50.right = _60;

		List<List<BinaryTreeNode>> levels = underTest.listChildren(_50);
		
		Assert.assertEquals(4, levels.size());
		Assert.assertEquals(1, levels.get(0).size());
		Assert.assertEquals(2, levels.get(1).size());
		Assert.assertEquals(4, levels.get(2).size());
		Assert.assertEquals(8, levels.get(3).size());
		
		Assert.assertEquals(new Integer(50), levels.get(0).get(0).key);

		Assert.assertEquals(new Integer(40), levels.get(1).get(0).key);
		Assert.assertEquals(new Integer(60), levels.get(1).get(1).key);

		Assert.assertEquals(new Integer(30), levels.get(2).get(0).key);
		Assert.assertEquals(new Integer(45), levels.get(2).get(1).key);
		Assert.assertEquals(new Integer(55), levels.get(2).get(2).key);
		Assert.assertEquals(new Integer(70), levels.get(2).get(3).key);

		Assert.assertEquals(new Integer(20), levels.get(3).get(0).key);
		Assert.assertEquals(new Integer(35), levels.get(3).get(1).key);
		Assert.assertEquals(new Integer(43), levels.get(3).get(2).key);
		Assert.assertEquals(new Integer(49), levels.get(3).get(3).key);
		Assert.assertEquals(new Integer(51), levels.get(3).get(4).key);
		Assert.assertEquals(new Integer(59), levels.get(3).get(5).key);
		Assert.assertEquals(new Integer(69), levels.get(3).get(6).key);
		Assert.assertEquals(new Integer(80), levels.get(3).get(7).key);
	}

}