package com.anoop.ds.trees;

import org.junit.Assert;
import org.junit.Test;

public class TreeBalancedCheckerTest {

	TreeBalancedChecker underTest = new TreeBalancedChecker();
	
	@Test
	public void testTreeIsBalancedAtDepth0() {
		BinaryTreeNode root = new BinaryTreeNode(10, "10");
		Assert.assertTrue(underTest.isBalanced(root));
	}

	@Test
	public void testTreeIsBalancedAtDepth1() {
		BinaryTreeNode root = new BinaryTreeNode(10, "10");
		BinaryTreeNode _5 = new BinaryTreeNode(5, "5");
		BinaryTreeNode _15 = new BinaryTreeNode(15, "15");
		root.left = _5;
		root.right = _15;
		Assert.assertTrue(underTest.isBalanced(root));
	}

	@Test
	public void testTreeIsBalancedAtDepth1WithDifferenceOfOneDepth() {
		BinaryTreeNode root = new BinaryTreeNode(10, "10");
		BinaryTreeNode _5 = new BinaryTreeNode(5, "5");
		BinaryTreeNode _15 = new BinaryTreeNode(15, "15");
		BinaryTreeNode _20 = new BinaryTreeNode(20, "20");
		
		_15.right = _20;
		root.left = _5;
		root.right = _15;
		
		Assert.assertTrue(underTest.isBalanced(root));
	}

	@Test
	public void testTreeIsNotBalancedAtDepth1() {
		BinaryTreeNode root = new BinaryTreeNode(10, "10");
		BinaryTreeNode _5 = new BinaryTreeNode(5, "5");
		BinaryTreeNode _15 = new BinaryTreeNode(15, "15");
		BinaryTreeNode _20 = new BinaryTreeNode(20, "20");
		BinaryTreeNode _30 = new BinaryTreeNode(30, "30");
		
		_20.right = _30;
		_15.right = _20;
		root.left = _5;
		root.right = _15;
		
		Assert.assertFalse(underTest.isBalanced(root));
	}

	@Test
	public void testTreeIsBalancedAtDepth2() {
		BinaryTreeNode root = new BinaryTreeNode(10, "10");
		
		BinaryTreeNode _5 = new BinaryTreeNode(5, "5");
		BinaryTreeNode _15 = new BinaryTreeNode(15, "15");
		
		BinaryTreeNode _3 = new BinaryTreeNode(5, "5");
		BinaryTreeNode _7 = new BinaryTreeNode(15, "15");
		BinaryTreeNode _13 = new BinaryTreeNode(5, "5");
		BinaryTreeNode _17 = new BinaryTreeNode(15, "15");

		
		_5.left = _3;
		_5.right = _7;
		_15.left = _13;
		_15.right = _17;
		
		root.left = _5;
		root.right = _15;
		
		Assert.assertTrue(underTest.isBalanced(root));
	}

	@Test
	public void testTreeIsBalancedAtDepth2WithDifferenceOfOneDepth() {
		
		BinaryTreeNode root = new BinaryTreeNode(10, "10");
		
		BinaryTreeNode _5 = new BinaryTreeNode(5, "5");
		BinaryTreeNode _15 = new BinaryTreeNode(15, "15");
		
		BinaryTreeNode _3 = new BinaryTreeNode(5, "5");
		BinaryTreeNode _7 = new BinaryTreeNode(15, "15");
		BinaryTreeNode _13 = new BinaryTreeNode(13, "13");
		BinaryTreeNode _17 = new BinaryTreeNode(15, "15");

		BinaryTreeNode _20 = new BinaryTreeNode(20, "20");

		
		_17.right = _20;
		
		_5.left = _3;
		_5.right = _7;
		_15.left = _13;
		_15.right = _17;
		
		root.left = _5;
		root.right = _15;
		
		Assert.assertTrue(underTest.isBalanced(root));
	}

	@Test
	public void testTreeIsNotBalancedAtDepth2() {
		
		BinaryTreeNode root = new BinaryTreeNode(10, "10");
		
		BinaryTreeNode _5 = new BinaryTreeNode(5, "5");
		BinaryTreeNode _15 = new BinaryTreeNode(15, "15");
		
		BinaryTreeNode _3 = new BinaryTreeNode(5, "5");
		BinaryTreeNode _7 = new BinaryTreeNode(15, "15");
		BinaryTreeNode _13 = new BinaryTreeNode(13, "13");
		BinaryTreeNode _17 = new BinaryTreeNode(15, "15");

		BinaryTreeNode _20 = new BinaryTreeNode(20, "20");

		BinaryTreeNode _25 = new BinaryTreeNode(25, "25");

		_20.right = _25;
		_17.right = _20;
		
		_5.left = _3;
		_5.right = _7;
		_15.left = _13;
		_15.right = _17;
		
		root.left = _5;
		root.right = _15;
		
		Assert.assertFalse(underTest.isBalanced(root));
	}

	@Test
	public void testTreeIsBalancedAtDepth3() {
		BinaryTreeNode root = new BinaryTreeNode(10, "10");
		
		BinaryTreeNode _5 = new BinaryTreeNode(5, "5");
		BinaryTreeNode _15 = new BinaryTreeNode(15, "15");
		
		BinaryTreeNode _3 = new BinaryTreeNode(5, "5");
		BinaryTreeNode _7 = new BinaryTreeNode(15, "15");
		BinaryTreeNode _13 = new BinaryTreeNode(13, "13");
		BinaryTreeNode _17 = new BinaryTreeNode(17, "17");

		
		_5.left = _3;
		_5.right = _7;
		_15.left = _13;
		_15.right = _17;
		
		root.left = _5;
		root.right = _15;
		
		Assert.assertTrue(underTest.isBalanced(root));
	}

	@Test
	public void testTreeIsBalancedAtDepth4() {
		
		BinaryTreeNode _13 = new BinaryTreeNode(13, "13");
		BinaryTreeNode _30 = new BinaryTreeNode(30, "30");
		BinaryTreeNode _70 = new BinaryTreeNode(70, "70");
		BinaryTreeNode _90 = new BinaryTreeNode(90, "90");
		BinaryTreeNode _121 = new BinaryTreeNode(121, "121"); //<- Differing node
		BinaryTreeNode _140 = new BinaryTreeNode(140, "140");
		BinaryTreeNode _170 = new BinaryTreeNode(170, "170");
		BinaryTreeNode _200 = new BinaryTreeNode(200, "200");
		
		BinaryTreeNode _25 = new BinaryTreeNode(25, "25");
		BinaryTreeNode _75 = new BinaryTreeNode(75, "75");
		BinaryTreeNode _125 = new BinaryTreeNode(125, "125");
		BinaryTreeNode _175 = new BinaryTreeNode(175, "175");
		
		BinaryTreeNode _50 = new BinaryTreeNode(50, "50");
		BinaryTreeNode _150 = new BinaryTreeNode(150, "150");

		BinaryTreeNode _100 = new BinaryTreeNode(100, "100");
		
		_25.left = _13;
		_25.right = _30;
		_75.left = _70;
		_75.right = _90;
		_125.left = _121;
		_125.right = _140;
		_175.left = _170;
		_175.right = _200;
		_50.left = _25;
		_50.right = _75;
		_150.left = _125;
		_150.right = _175;
		_100.left = _50;
		_100.right = _150;
		
		Assert.assertTrue(underTest.isBalanced(_100));
	}

	@Test
	public void testTreeIsBalancedAtDepth4WithOneExtraLevelOfDepth() {
		
		BinaryTreeNode _250 = new BinaryTreeNode(250, "250");
		
		BinaryTreeNode _13 = new BinaryTreeNode(13, "13");
		BinaryTreeNode _30 = new BinaryTreeNode(30, "30");
		BinaryTreeNode _70 = new BinaryTreeNode(70, "70");
		BinaryTreeNode _90 = new BinaryTreeNode(90, "90");
		BinaryTreeNode _121 = new BinaryTreeNode(121, "121"); //<- Differing node
		BinaryTreeNode _140 = new BinaryTreeNode(140, "140");
		BinaryTreeNode _170 = new BinaryTreeNode(170, "170");
		BinaryTreeNode _200 = new BinaryTreeNode(200, "200");
		
		BinaryTreeNode _25 = new BinaryTreeNode(25, "25");
		BinaryTreeNode _75 = new BinaryTreeNode(75, "75");
		BinaryTreeNode _125 = new BinaryTreeNode(125, "125");
		BinaryTreeNode _175 = new BinaryTreeNode(175, "175");
		
		BinaryTreeNode _50 = new BinaryTreeNode(50, "50");
		BinaryTreeNode _150 = new BinaryTreeNode(150, "150");

		BinaryTreeNode _100 = new BinaryTreeNode(100, "100");
		
		_200.right = _250;
		_25.left = _13;
		_25.right = _30;
		_75.left = _70;
		_75.right = _90;
		_125.left = _121;
		_125.right = _140;
		_175.left = _170;
		_175.right = _200;
		_50.left = _25;
		_50.right = _75;
		_150.left = _125;
		_150.right = _175;
		_100.left = _50;
		_100.right = _150;
		
		Assert.assertTrue(underTest.isBalanced(_100));
	}

	@Test
	public void testTreeIsUnbalancedBalancedAtDepth4() {
		
		BinaryTreeNode _300 = new BinaryTreeNode(300, "300");
		
		BinaryTreeNode _250 = new BinaryTreeNode(250, "250");
		
		BinaryTreeNode _13 = new BinaryTreeNode(13, "13");
		BinaryTreeNode _30 = new BinaryTreeNode(30, "30");
		BinaryTreeNode _70 = new BinaryTreeNode(70, "70");
		BinaryTreeNode _90 = new BinaryTreeNode(90, "90");
		BinaryTreeNode _121 = new BinaryTreeNode(121, "121"); //<- Differing node
		BinaryTreeNode _140 = new BinaryTreeNode(140, "140");
		BinaryTreeNode _170 = new BinaryTreeNode(170, "170");
		BinaryTreeNode _200 = new BinaryTreeNode(200, "200");
		
		BinaryTreeNode _25 = new BinaryTreeNode(25, "25");
		BinaryTreeNode _75 = new BinaryTreeNode(75, "75");
		BinaryTreeNode _125 = new BinaryTreeNode(125, "125");
		BinaryTreeNode _175 = new BinaryTreeNode(175, "175");
		
		BinaryTreeNode _50 = new BinaryTreeNode(50, "50");
		BinaryTreeNode _150 = new BinaryTreeNode(150, "150");

		BinaryTreeNode _100 = new BinaryTreeNode(100, "100");
		
		_250.right = _300;
		_200.right = _250;
		_25.left = _13;
		_25.right = _30;
		_75.left = _70;
		_75.right = _90;
		_125.left = _121;
		_125.right = _140;
		_175.left = _170;
		_175.right = _200;
		_50.left = _25;
		_50.right = _75;
		_150.left = _125;
		_150.right = _175;
		_100.left = _50;
		_100.right = _150;
		
		Assert.assertFalse(underTest.isBalanced(_100));
	}

	@Test
	public void testTreeIsBalancedAtDepth5() {
		
		BinaryTreeNode _10 = new BinaryTreeNode(10, "10");
		BinaryTreeNode _15 = new BinaryTreeNode(15, "15");
		
		BinaryTreeNode _13 = new BinaryTreeNode(13, "13");
		BinaryTreeNode _30 = new BinaryTreeNode(30, "30");
		BinaryTreeNode _70 = new BinaryTreeNode(70, "70");
		BinaryTreeNode _90 = new BinaryTreeNode(90, "90");
		BinaryTreeNode _121 = new BinaryTreeNode(121, "121"); //<- Differing node
		BinaryTreeNode _140 = new BinaryTreeNode(140, "140");
		BinaryTreeNode _170 = new BinaryTreeNode(170, "170");
		BinaryTreeNode _200 = new BinaryTreeNode(200, "200");
		
		BinaryTreeNode _25 = new BinaryTreeNode(25, "25");
		BinaryTreeNode _75 = new BinaryTreeNode(75, "75");
		BinaryTreeNode _125 = new BinaryTreeNode(125, "125");
		BinaryTreeNode _175 = new BinaryTreeNode(175, "175");
		
		BinaryTreeNode _50 = new BinaryTreeNode(50, "50");
		BinaryTreeNode _150 = new BinaryTreeNode(150, "150");

		BinaryTreeNode _100 = new BinaryTreeNode(100, "100");
		
		_13.left = _10;
		_13.right = _15;
		_25.left = _13;
		_25.right = _30;
		_75.left = _70;
		_75.right = _90;
		_125.left = _121;
		_125.right = _140;
		_175.left = _170;
		_175.right = _200;
		_50.left = _25;
		_50.right = _75;
		_150.left = _125;
		_150.right = _175;
		_100.left = _50;
		_100.right = _150;
		
		Assert.assertTrue(underTest.isBalanced(_100));
	}

}
