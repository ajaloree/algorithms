package com.anoop.ds.trees;

import java.util.List;

import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class BinaryTreePathFinderTest {

	BinaryTreePathFinder underTest;
	
	@Before
	public void init() {
		
		Node a = new Node("A");
		Node b = new Node("B");
		Node c = new Node("C");
		Node d = new Node("D");
		Node e = new Node("E");
		Node f = new Node("F");
		Node g = new Node("G");
		
		a.left = b;
		a.right = c;
		
		b.left = d;
		b.right = e;
		
		c.left = f;
		c.right = g;
		
		underTest = new BinaryTreePathFinder(a);
	}

	@Test
	public void testPathFromRootToNode() {
		Assert.assertEquals("ABD", underTest.getPathFromRootToNode("D"));
		Assert.assertEquals("ABE", underTest.getPathFromRootToNode("E"));
		Assert.assertEquals("ACF", underTest.getPathFromRootToNode("F"));
		Assert.assertEquals("ACG", underTest.getPathFromRootToNode("G"));
		Assert.assertEquals("AB", underTest.getPathFromRootToNode("B"));
		Assert.assertEquals("AC", underTest.getPathFromRootToNode("C"));
		Assert.assertEquals("A", underTest.getPathFromRootToNode("A"));
		Assert.assertEquals("Node not present in tree", underTest.getPathFromRootToNode("X"));
	}
	
	@Test
	public void testAllPathsFromRoot() {
		List<String> allPathsFromRoot = underTest.getAllPathsFromRoot();
		Assert.assertThat(allPathsFromRoot, Matchers.containsInAnyOrder("ABD","ABE","ACF","ACG"));
	}
	
	@Test
	public void testPathBetweenNodes() {
		Assert.assertEquals("A", underTest.getPathBetweenNodes("A", "A"));
		Assert.assertEquals("BAC", underTest.getPathBetweenNodes("B", "C"));
		Assert.assertEquals("DBACG", underTest.getPathBetweenNodes("D", "G"));
		Assert.assertEquals("EBACF", underTest.getPathBetweenNodes("E", "F"));
		Assert.assertEquals("BACG", underTest.getPathBetweenNodes("B", "G"));
		Assert.assertEquals("BACF", underTest.getPathBetweenNodes("B", "F"));
		Assert.assertEquals("CABD", underTest.getPathBetweenNodes("C", "D"));
		Assert.assertEquals("CABE", underTest.getPathBetweenNodes("C", "E"));
	}
}
