package com.anoop.ds.trees;

import org.junit.Assert;
import org.junit.Test;

public class TreeDepthTest {

	TreeDepth underTest = new TreeDepth();
	
	@Test
	public void testDepth0() {
		BinaryTreeNode root = new BinaryTreeNode(10, "10");
		Assert.assertEquals(0, underTest.depth(root));
	}

	@Test
	public void testDepth1() {
		BinaryTreeNode root = new BinaryTreeNode(10, "10");
		BinaryTreeNode _5 = new BinaryTreeNode(5, "5");
		BinaryTreeNode _15 = new BinaryTreeNode(15, "15");
		root.left = _5;
		root.right = _15;
		Assert.assertEquals(1, underTest.depth(root));
	}

	@Test
	public void testDepth2() {
		BinaryTreeNode root = new BinaryTreeNode(10, "10");
		
		BinaryTreeNode _5 = new BinaryTreeNode(5, "5");
		BinaryTreeNode _15 = new BinaryTreeNode(15, "15");
		
		BinaryTreeNode _3 = new BinaryTreeNode(5, "5");
		BinaryTreeNode _7 = new BinaryTreeNode(15, "15");
		BinaryTreeNode _13 = new BinaryTreeNode(13, "13");
		BinaryTreeNode _17 = new BinaryTreeNode(17, "17");

		
		_5.left = _3;
		_5.right = _7;
		_15.left = _13;
		_15.right = _17;
		
		root.left = _5;
		root.right = _15;
		
		Assert.assertEquals(2, underTest.depth(root));
	}

	@Test
	public void testDepth3() {
		BinaryTreeNode root = new BinaryTreeNode(10, "10");
		
		BinaryTreeNode _5 = new BinaryTreeNode(5, "5");
		BinaryTreeNode _15 = new BinaryTreeNode(15, "15");
		
		BinaryTreeNode _3 = new BinaryTreeNode(5, "5");
		BinaryTreeNode _7 = new BinaryTreeNode(15, "15");
		BinaryTreeNode _13 = new BinaryTreeNode(13, "13");
		BinaryTreeNode _17 = new BinaryTreeNode(17, "17");

		
		_5.left = _3;
		_5.right = _7;
		_15.left = _13;
		_15.right = _17;
		
		root.left = _5;
		root.right = _15;
		
		Assert.assertEquals(2, underTest.depth(root));
	}

	@Test
	public void testDepth4() {
		
		BinaryTreeNode _13 = new BinaryTreeNode(13, "13");
		BinaryTreeNode _30 = new BinaryTreeNode(30, "30");
		BinaryTreeNode _70 = new BinaryTreeNode(70, "70");
		BinaryTreeNode _90 = new BinaryTreeNode(90, "90");
		BinaryTreeNode _121 = new BinaryTreeNode(121, "121"); //<- Differing node
		BinaryTreeNode _140 = new BinaryTreeNode(140, "140");
		BinaryTreeNode _170 = new BinaryTreeNode(170, "170");
		BinaryTreeNode _200 = new BinaryTreeNode(200, "200");
		
		BinaryTreeNode _25 = new BinaryTreeNode(25, "25");
		BinaryTreeNode _75 = new BinaryTreeNode(75, "75");
		BinaryTreeNode _125 = new BinaryTreeNode(125, "125");
		BinaryTreeNode _175 = new BinaryTreeNode(175, "175");
		
		BinaryTreeNode _50 = new BinaryTreeNode(50, "50");
		BinaryTreeNode _150 = new BinaryTreeNode(150, "150");

		BinaryTreeNode _100 = new BinaryTreeNode(100, "100");
		
		_25.left = _13;
		_25.right = _30;
		_75.left = _70;
		_75.right = _90;
		_125.left = _121;
		_125.right = _140;
		_175.left = _170;
		_175.right = _200;
		_50.left = _25;
		_50.right = _75;
		_150.left = _125;
		_150.right = _175;
		_100.left = _50;
		_100.right = _150;
		
		Assert.assertEquals(3, underTest.depth(_100));
	}

	@Test
	public void testDepth5() {
		
		BinaryTreeNode _10 = new BinaryTreeNode(10, "10");
		BinaryTreeNode _15 = new BinaryTreeNode(15, "15");
		
		BinaryTreeNode _13 = new BinaryTreeNode(13, "13");
		BinaryTreeNode _30 = new BinaryTreeNode(30, "30");
		BinaryTreeNode _70 = new BinaryTreeNode(70, "70");
		BinaryTreeNode _90 = new BinaryTreeNode(90, "90");
		BinaryTreeNode _121 = new BinaryTreeNode(121, "121"); //<- Differing node
		BinaryTreeNode _140 = new BinaryTreeNode(140, "140");
		BinaryTreeNode _170 = new BinaryTreeNode(170, "170");
		BinaryTreeNode _200 = new BinaryTreeNode(200, "200");
		
		BinaryTreeNode _25 = new BinaryTreeNode(25, "25");
		BinaryTreeNode _75 = new BinaryTreeNode(75, "75");
		BinaryTreeNode _125 = new BinaryTreeNode(125, "125");
		BinaryTreeNode _175 = new BinaryTreeNode(175, "175");
		
		BinaryTreeNode _50 = new BinaryTreeNode(50, "50");
		BinaryTreeNode _150 = new BinaryTreeNode(150, "150");

		BinaryTreeNode _100 = new BinaryTreeNode(100, "100");
		
		_13.left = _10;
		_13.right = _15;
		_25.left = _13;
		_25.right = _30;
		_75.left = _70;
		_75.right = _90;
		_125.left = _121;
		_125.right = _140;
		_175.left = _170;
		_175.right = _200;
		_50.left = _25;
		_50.right = _75;
		_150.left = _125;
		_150.right = _175;
		_100.left = _50;
		_100.right = _150;
		
		Assert.assertEquals(4, underTest.depth(_100));
	}

}
