package com.anoop.ds.trees;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class SubtreeCheckerTest {
	
	SubtreeChecker underTest = new SubtreeChecker();
	BinaryTreeNode rootOfLargeTree;
	
	@Before
	public void getLargeTree() {
		
		BinaryTreeNode _5 = new BinaryTreeNode(5, "5");
		BinaryTreeNode _20 = new BinaryTreeNode(20, "20");
		BinaryTreeNode _28 = new BinaryTreeNode(28, "28");
		BinaryTreeNode _32 = new BinaryTreeNode(32, "32");
		BinaryTreeNode _68 = new BinaryTreeNode(68, "68");
		BinaryTreeNode _72 = new BinaryTreeNode(72, "72");
		BinaryTreeNode _88 = new BinaryTreeNode(88, "88");
		BinaryTreeNode _92 = new BinaryTreeNode(92, "92");
		BinaryTreeNode _118 = new BinaryTreeNode(118, "118");
		BinaryTreeNode _122 = new BinaryTreeNode(122, "122");
		BinaryTreeNode _138 = new BinaryTreeNode(138, "138");
		BinaryTreeNode _142 = new BinaryTreeNode(142, "142");
		
		BinaryTreeNode _13 = new BinaryTreeNode(13, "13");
		BinaryTreeNode _30 = new BinaryTreeNode(30, "30");
		BinaryTreeNode _70 = new BinaryTreeNode(70, "70");
		BinaryTreeNode _90 = new BinaryTreeNode(90, "90");
		BinaryTreeNode _120 = new BinaryTreeNode(120, "120");
		BinaryTreeNode _140 = new BinaryTreeNode(140, "140");
		BinaryTreeNode _170 = new BinaryTreeNode(170, "170");
		BinaryTreeNode _200 = new BinaryTreeNode(200, "200");
		
		BinaryTreeNode _25 = new BinaryTreeNode(25, "25");
		BinaryTreeNode _75 = new BinaryTreeNode(75, "75");
		BinaryTreeNode _125 = new BinaryTreeNode(125, "125");
		BinaryTreeNode _175 = new BinaryTreeNode(175, "175");
		
		BinaryTreeNode _50 = new BinaryTreeNode(50, "50");
		BinaryTreeNode _150 = new BinaryTreeNode(150, "150");

		BinaryTreeNode _100 = new BinaryTreeNode(100, "100");

		//Associating nodes at depth-3 with those at depth-4.
		_13.left = _5;
		_13.right = _20;
		_30.left = _28;
		_30.right = _32;
		_70.left = _68;
		_70.right = _72;
		_90.left = _88;
		_90.right = _92;
		_120.left = _118;
		_120.right = _122;
		_140.left = _138;
		_140.right = _142;
		
		//Associating nodes at depth-2 with those at depth-3.
		_25.left = _13;
		_25.right = _30;
		_75.left = _70;
		_75.right = _90;
		_125.left = _120;
		_125.right = _140;
		_175.left = _170;
		_175.right = _200;

		//Associating nodes at depth-1 with those at depth-2.
		_50.left = _25;
		_50.right = _75;
		_150.left = _125;
		_150.right = _175;

		//Associating nodes at depth-0 with those at depth-1.
		_100.left = _50;
		_100.right = _150;

		rootOfLargeTree = _100;
	}
	
	@Test
	public void testSubNodesWithinTheSameTreeAreRecognisedAsSubTrees() {
	
		BinaryTreeNode _5 = new BinaryTreeNode(5, "5");
		BinaryTreeNode _20 = new BinaryTreeNode(20, "20");
		BinaryTreeNode _28 = new BinaryTreeNode(28, "28");
		BinaryTreeNode _32 = new BinaryTreeNode(32, "32");
		BinaryTreeNode _68 = new BinaryTreeNode(68, "68");
		BinaryTreeNode _72 = new BinaryTreeNode(72, "72");
		BinaryTreeNode _88 = new BinaryTreeNode(88, "88");
		BinaryTreeNode _92 = new BinaryTreeNode(92, "92");
		BinaryTreeNode _118 = new BinaryTreeNode(118, "118");
		BinaryTreeNode _122 = new BinaryTreeNode(122, "122");
		BinaryTreeNode _138 = new BinaryTreeNode(138, "138");
		BinaryTreeNode _142 = new BinaryTreeNode(142, "142");
		
		BinaryTreeNode _13 = new BinaryTreeNode(13, "13");
		BinaryTreeNode _30 = new BinaryTreeNode(30, "30");
		BinaryTreeNode _70 = new BinaryTreeNode(70, "70");
		BinaryTreeNode _90 = new BinaryTreeNode(90, "90");
		BinaryTreeNode _120 = new BinaryTreeNode(120, "120");
		BinaryTreeNode _140 = new BinaryTreeNode(140, "140");
		BinaryTreeNode _170 = new BinaryTreeNode(170, "170");
		BinaryTreeNode _200 = new BinaryTreeNode(200, "200");
		
		BinaryTreeNode _25 = new BinaryTreeNode(25, "25");
		BinaryTreeNode _75 = new BinaryTreeNode(75, "75");
		BinaryTreeNode _125 = new BinaryTreeNode(125, "125");
		BinaryTreeNode _175 = new BinaryTreeNode(175, "175");
		
		BinaryTreeNode _50 = new BinaryTreeNode(50, "50");
		BinaryTreeNode _150 = new BinaryTreeNode(150, "150");

		BinaryTreeNode _100 = new BinaryTreeNode(100, "100");

		//Associating nodes at depth-3 with those at depth-4.
		_13.left = _5;
		_13.right = _20;
		_30.left = _28;
		_30.right = _32;
		_70.left = _68;
		_70.right = _72;
		_90.left = _88;
		_90.right = _92;
		_120.left = _118;
		_120.right = _122;
		_140.left = _138;
		_140.right = _142;
		
		//Associating nodes at depth-2 with those at depth-3.
		_25.left = _13;
		_25.right = _30;
		_75.left = _70;
		_75.right = _90;
		_125.left = _120;
		_125.right = _140;
		_175.left = _170;
		_175.right = _200;

		//Associating nodes at depth-1 with those at depth-2.
		_50.left = _25;
		_50.right = _75;
		_150.left = _125;
		_150.right = _175;

		//Associating nodes at depth-0 with those at depth-1.
		_100.left = _50;
		_100.right = _150;
		
		Assert.assertTrue(underTest.isSubtree(_50, _100));
		Assert.assertTrue(underTest.isSubtree(_150, _100));
		Assert.assertTrue(underTest.isSubtree(_25, _100));
		Assert.assertTrue(underTest.isSubtree(_75, _100));
		Assert.assertTrue(underTest.isSubtree(_125, _100));
		Assert.assertTrue(underTest.isSubtree(_175, _100));
		Assert.assertTrue(underTest.isSubtree(_13, _100));
		Assert.assertTrue(underTest.isSubtree(_30, _100));
	}
	
	@Test
	public void testNoSubtreeCheckForTreeOfDepth1() {
		
		BinaryTreeNode _50 = new BinaryTreeNode(50, "50");
		BinaryTreeNode _151 = new BinaryTreeNode(151, "151"); //<- Differing node
		BinaryTreeNode _100 = new BinaryTreeNode(100, "100");
		_100.left = _50;
		_100.right = _151;
		
		Assert.assertFalse(underTest.isSubtree(_100, rootOfLargeTree));
	}

	@Test
	public void testNoSubtreeCheckForTreeOfDepth2() {
		
		BinaryTreeNode _25 = new BinaryTreeNode(25, "25");
		BinaryTreeNode _76 = new BinaryTreeNode(76, "76");  //<- Differing node
		BinaryTreeNode _125 = new BinaryTreeNode(125, "125");
		BinaryTreeNode _175 = new BinaryTreeNode(175, "175");
		BinaryTreeNode _50 = new BinaryTreeNode(50, "50");
		BinaryTreeNode _150 = new BinaryTreeNode(150, "150");
		BinaryTreeNode _100 = new BinaryTreeNode(100, "100");
		
		_50.left = _25;
		_50.right = _76;
		_150.left = _125;
		_150.right = _175;
		_100.left = _50;
		_100.right = _150;
		
		Assert.assertFalse(underTest.isSubtree(_100, rootOfLargeTree));
	}

	@Test
	public void testNoSubtreeCheckForTreeOfDepth3() {
		
		BinaryTreeNode _13 = new BinaryTreeNode(13, "13");
		BinaryTreeNode _30 = new BinaryTreeNode(30, "30");
		BinaryTreeNode _70 = new BinaryTreeNode(70, "70");
		BinaryTreeNode _90 = new BinaryTreeNode(90, "90");
		BinaryTreeNode _121 = new BinaryTreeNode(121, "121"); //<- Differing node
		BinaryTreeNode _140 = new BinaryTreeNode(140, "140");
		BinaryTreeNode _170 = new BinaryTreeNode(170, "170");
		BinaryTreeNode _200 = new BinaryTreeNode(200, "200");
		
		BinaryTreeNode _25 = new BinaryTreeNode(25, "25");
		BinaryTreeNode _75 = new BinaryTreeNode(75, "75");
		BinaryTreeNode _125 = new BinaryTreeNode(125, "125");
		BinaryTreeNode _175 = new BinaryTreeNode(175, "175");
		
		BinaryTreeNode _50 = new BinaryTreeNode(50, "50");
		BinaryTreeNode _150 = new BinaryTreeNode(150, "150");

		BinaryTreeNode _100 = new BinaryTreeNode(100, "100");
		
		_25.left = _13;
		_25.right = _30;
		_75.left = _70;
		_75.right = _90;
		_125.left = _121;
		_125.right = _140;
		_175.left = _170;
		_175.right = _200;
		_50.left = _25;
		_50.right = _75;
		_150.left = _125;
		_150.right = _175;
		_100.left = _50;
		_100.right = _150;
		
		Assert.assertFalse(underTest.isSubtree(_100, rootOfLargeTree));
	}

}
