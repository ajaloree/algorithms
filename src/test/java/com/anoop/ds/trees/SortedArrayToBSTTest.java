package com.anoop.ds.trees;

import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.Assert;
import org.junit.Test;

public class SortedArrayToBSTTest {

	SortedArrayToBST underTest = new SortedArrayToBST();

	@Test
	public void testWithOneElementList() {
		
		BinaryTreeNode root = underTest.convertToNode(Stream.of(1).collect(Collectors.toList()));
		Assert.assertEquals(root.key, new Integer(1));
		Assert.assertNull(root.left);
		Assert.assertNull(root.right);
	}

	@Test
	public void testWithTwoElementsList() {
		
		BinaryTreeNode root = underTest.convertToNode(Stream.of(1,2).collect(Collectors.toList()));
		Assert.assertEquals(root.key, new Integer(2));
		Assert.assertEquals(root.left.key, new Integer(1));
		Assert.assertNull(root.right);
	}

	@Test
	public void testWithThreeElementsList() {
		
		BinaryTreeNode root = underTest.convertToNode(Stream.of(1,2,3).collect(Collectors.toList()));
		Assert.assertEquals(root.key, new Integer(2));
		Assert.assertEquals(root.left.key, new Integer(1));
		Assert.assertEquals(root.right.key, new Integer(3));
	}

	@Test
	public void testWithFourElementsList() {
		
		BinaryTreeNode root = underTest.convertToNode(Stream.of(1,2,3,4).collect(Collectors.toList()));
		Assert.assertEquals(root.key, new Integer(3));
		Assert.assertEquals(root.left.key, new Integer(2));
		Assert.assertEquals(root.left.left.key, new Integer(1));
		Assert.assertEquals(root.right.key, new Integer(4));
	}

	@Test
	public void testWithFiveElementsList() {
		
		BinaryTreeNode root = underTest.convertToNode(Stream.of(1,2,3,4,5).collect(Collectors.toList()));
		Assert.assertEquals(root.key, new Integer(3));
		Assert.assertEquals(root.left.key, new Integer(2));
		Assert.assertEquals(root.left.left.key, new Integer(1));
		Assert.assertEquals(root.right.key, new Integer(5));
		Assert.assertEquals(root.right.left.key, new Integer(4));
	}

	@Test
	public void testWithSixElementsList() {
		
		BinaryTreeNode root = underTest.convertToNode(Stream.of(1,2,3,4,5,6).collect(Collectors.toList()));
		Assert.assertEquals(root.key, new Integer(4));
		Assert.assertEquals(root.left.key, new Integer(2));
		Assert.assertEquals(root.left.left.key, new Integer(1));
		Assert.assertEquals(root.left.right.key, new Integer(3));
		Assert.assertEquals(root.right.key, new Integer(6));
		Assert.assertEquals(root.right.left.key, new Integer(5));
	}

	@Test
	public void testWithSevenElementsList() {
		
		BinaryTreeNode root = underTest.convertToNode(Stream.of(1,2,3,4,5,6,7).collect(Collectors.toList()));
		Assert.assertEquals(root.key, new Integer(4));
		Assert.assertEquals(root.left.key, new Integer(2));
		Assert.assertEquals(root.left.left.key, new Integer(1));
		Assert.assertEquals(root.left.right.key, new Integer(3));
		Assert.assertEquals(root.right.key, new Integer(6));
		Assert.assertEquals(root.right.left.key, new Integer(5));
		Assert.assertEquals(root.right.right.key, new Integer(7));
	}

	@Test
	public void testWithEightElementsList() {
		
		BinaryTreeNode root = underTest.convertToNode(Stream.of(1,2,3,4,5,6,7,8).collect(Collectors.toList()));
		Assert.assertEquals(root.key, new Integer(5));
		Assert.assertEquals(root.left.key, new Integer(3));
		Assert.assertEquals(root.left.left.key, new Integer(2));
		Assert.assertEquals(root.left.left.left.key, new Integer(1));
		Assert.assertEquals(root.left.right.key, new Integer(4));
		Assert.assertEquals(root.right.key, new Integer(7));
		Assert.assertEquals(root.right.left.key, new Integer(6));
		Assert.assertEquals(root.right.right.key, new Integer(8));
	}

	@Test
	public void testWithNineElementsList() {
		
		BinaryTreeNode root = underTest.convertToNode(Stream.of(1,2,3,4,5,6,7,8,9).collect(Collectors.toList()));
		Assert.assertEquals(root.key, new Integer(5));
		Assert.assertEquals(root.left.key, new Integer(3));
		Assert.assertEquals(root.left.left.key, new Integer(2));
		Assert.assertEquals(root.left.left.left.key, new Integer(1));
		Assert.assertEquals(root.left.right.key, new Integer(4));
		Assert.assertEquals(root.right.key, new Integer(8));
		Assert.assertEquals(root.right.left.key, new Integer(7));
		Assert.assertEquals(root.right.left.left.key, new Integer(6));
		Assert.assertEquals(root.right.right.key, new Integer(9));
	}

	@Test
	public void testWithTenElementsList() {
		
		BinaryTreeNode root = underTest.convertToNode(Stream.of(1,2,3,4,5,6,7,8,9,10).collect(Collectors.toList()));
		Assert.assertEquals(root.key, new Integer(6));
		
		Assert.assertEquals(root.left.key, new Integer(3));
		Assert.assertEquals(root.right.key, new Integer(9));

		Assert.assertEquals(root.left.left.key, new Integer(2));
		Assert.assertEquals(root.left.left.left.key, new Integer(1));
		Assert.assertNull(root.left.left.right);

		Assert.assertEquals(root.right.left.key, new Integer(8));
		Assert.assertEquals(root.right.right.key, new Integer(10));
		Assert.assertEquals(root.right.left.left.key, new Integer(7));
		Assert.assertNull(root.right.left.right);
		Assert.assertNull(root.right.right.left);
		Assert.assertNull(root.right.right.right);
	}
}
