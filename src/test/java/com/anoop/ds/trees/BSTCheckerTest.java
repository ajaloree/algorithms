package com.anoop.ds.trees;

import org.junit.Assert;
import org.junit.Test;

public class BSTCheckerTest {

	BSTChecker underTest = new BSTChecker();
	
	@Test
	public void testBinaryTreeIsNotBST() {
		
		BinaryTreeNode n10 = new BinaryTreeNode(10, "Ten");
		BinaryTreeNode n5 = new BinaryTreeNode(5, "Five");
		BinaryTreeNode n15 = new BinaryTreeNode(15, "Fifteen");
		BinaryTreeNode n3 = new BinaryTreeNode(3, "Three");
		BinaryTreeNode n2 = new BinaryTreeNode(2, "Two");
		BinaryTreeNode n7 = new BinaryTreeNode(7, "Seven");
		BinaryTreeNode n20 = new BinaryTreeNode(20, "Twenty");
		
		n10.left = n5;
		n10.right = n15;
		
		n5.left = n3;
		n5.right = n2;
		
		n15.left = n7;
		n15.right = n20;
		
		Assert.assertFalse(underTest.isBST(n10));
	}

	@Test
	public void testBinaryTreeIsBST() {
		
		BinaryTreeNode n10 = new BinaryTreeNode(10, "Ten");
		BinaryTreeNode n5 = new BinaryTreeNode(5, "Five");
		BinaryTreeNode n15 = new BinaryTreeNode(15, "Fifteen");
		BinaryTreeNode n3 = new BinaryTreeNode(3, "Three");
		BinaryTreeNode n6 = new BinaryTreeNode(6, "Six");
		BinaryTreeNode n13 = new BinaryTreeNode(13, "Thirteen");
		BinaryTreeNode n20 = new BinaryTreeNode(20, "Twenty");
		
		n10.left = n5;
		n10.right = n15;
		
		n5.left = n3;
		n5.right = n6;
		
		n15.left = n13;
		n15.right = n20;
		
		Assert.assertTrue(underTest.isBST(n10));
	}

	@Test
	public void testBinaryTreeIsBSTWhenTreeHasOnlyRightNodes() {
		
		BinaryTreeNode n1 = new BinaryTreeNode(1, "One");
		BinaryTreeNode n5 = new BinaryTreeNode(5, "Five");
		BinaryTreeNode n7 = new BinaryTreeNode(7, "Seven");
		BinaryTreeNode n10 = new BinaryTreeNode(10, "Ten");
		BinaryTreeNode n13 = new BinaryTreeNode(13, "Thirteen");
		
		n1.right = n5;
		n5.right = n7;
		n7.right = n10;		
		n10.right = n13;
		
		Assert.assertTrue(underTest.isBST(n1));
	}

	@Test
	public void testBinaryTreeIsNotBSTForATreeWithIncorrectOrdeningOfRightNodes() {
		
		BinaryTreeNode n1 = new BinaryTreeNode(1, "One");
		BinaryTreeNode n5 = new BinaryTreeNode(5, "Five");
		BinaryTreeNode n7 = new BinaryTreeNode(7, "Seven");
		BinaryTreeNode n10 = new BinaryTreeNode(10, "Ten");
		BinaryTreeNode n13 = new BinaryTreeNode(13, "Thirteen");
		
		n1.right = n5;
		n5.right = n10;
		n10.right = n7;		
		n7.right = n13;
		
		Assert.assertFalse(underTest.isBST(n1));
	}

}
