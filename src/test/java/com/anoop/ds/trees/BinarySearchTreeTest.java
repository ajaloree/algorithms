package com.anoop.ds.trees;

public class BinarySearchTreeTest {

	public static void main(String[] args) {
		
		BinarySearchTree<Integer, String> tree = new BinarySearchTree<Integer, String>();
		
		tree.put(10, "Ten");
		tree.put(5, "Five");
		tree.put(15, "Fifteen");
		tree.put(2, "Two");
		tree.put(7, "Seven");
		tree.put(13, "Thirteen");
		tree.put(17, "Seventeen");
		
		System.out.println("10: "+tree.get(10));
		System.out.println("5: "+tree.get(5));
		System.out.println("15: "+tree.get(15));
		System.out.println("2: "+tree.get(2));
		System.out.println("7: "+tree.get(7));
		System.out.println("13: "+tree.get(13));
		System.out.println("17: "+tree.get(17));

		System.out.println("11: "+tree.get(11));
		System.out.println("6: "+tree.get(6));
		System.out.println("16: "+tree.get(16));
		System.out.println("3: "+tree.get(3));
		System.out.println("8: "+tree.get(8));
		System.out.println("14: "+tree.get(14));
		System.out.println("18: "+tree.get(18));
		
		tree.inOrderTraversal();

		tree.preOrderTraversal();

		tree.postOrderTraversal();
		
		tree.breadthFirstTraversal();
		
		tree.printMin();

		tree.printMax();

	}
}
