package com.anoop.ds.list2;

import java.util.ListIterator;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class DoublyLinkedListTest {
	
	DoublyLinkedList<String> underTest;
	
	@Before
	public void init() {
		underTest = new DoublyLinkedList<>();
	}
	
	@Test
	public void testAddAndGet() {
		underTest.add("a");
		Assert.assertEquals(1, underTest.size());
		Assert.assertEquals("a", underTest.get(0));

		underTest.add("b");
		Assert.assertEquals(2, underTest.size());
		Assert.assertEquals("a", underTest.get(0));
		Assert.assertEquals("b", underTest.get(1));

		underTest.add("c");
		Assert.assertEquals(3, underTest.size());
		Assert.assertEquals("a", underTest.get(0));
		Assert.assertEquals("b", underTest.get(1));
		Assert.assertEquals("c", underTest.get(2));
	}

	@Test(expected = IndexOutOfBoundsException.class)
	public void testGetThrowsExceptionOnEmptyList() {
		underTest.get(0);
	}

	@Test(expected = IndexOutOfBoundsException.class)
	public void testGetThrowsExceptionIfIndexOutOfRange() {
		underTest.add("a");
		underTest.get(1);
	}
	
	@Test
	public void testSetAtIndex() {
		// set at beginning
		underTest.add("d");
		underTest.set(0, "a");
		Assert.assertEquals("a", underTest.get(0));
		Assert.assertEquals("d", underTest.get(1));

		// set at end
		underTest.set(1, "b");
		Assert.assertEquals(3, underTest.size());
		Assert.assertEquals("a", underTest.get(0));
		Assert.assertEquals("b", underTest.get(1));
		Assert.assertEquals("d", underTest.get(2));

		// set in middle
		underTest.set(2, "c");
		Assert.assertEquals(4, underTest.size());
		Assert.assertEquals("a", underTest.get(0));
		Assert.assertEquals("b", underTest.get(1));
		Assert.assertEquals("c", underTest.get(2));
		Assert.assertEquals("d", underTest.get(3));
	}
	
	@Test
	public void testIndexOf() {
		
		underTest.add("a");
		underTest.add("a");
		underTest.add("b");
		Assert.assertEquals(0, underTest.indexOf("a"));
		Assert.assertEquals(2, underTest.indexOf("b"));
		Assert.assertEquals(-1, underTest.indexOf("c"));
	}

	@Test
	public void testContains() {
		
		underTest.add("a");
		underTest.add("b");
		Assert.assertTrue(underTest.contains("a"));
		Assert.assertTrue(underTest.contains("b"));
		Assert.assertFalse(underTest.contains("c"));
	}
	
	@Test
	public void testDeleteByIndex() {

		underTest.add("a");
		Assert.assertEquals(1, underTest.size());
		underTest.delete(0);
		Assert.assertEquals(0, underTest.size());

		underTest.add("a");
		underTest.add("b");
		Assert.assertEquals(2, underTest.size());
		underTest.delete(0);
		Assert.assertEquals(1, underTest.size());
		Assert.assertEquals("b", underTest.get(0));
		underTest.delete(0);
		Assert.assertEquals(0, underTest.size());

		underTest.add("a");
		underTest.add("b");
		underTest.add("c");
		underTest.delete(1);
		Assert.assertEquals(2, underTest.size());
		Assert.assertEquals("a", underTest.get(0));
		Assert.assertEquals("c", underTest.get(1));
	}

	@Test
	public void testDeleteByValue() {

		underTest.add("a");
		Assert.assertEquals(1, underTest.size());
		Assert.assertTrue(underTest.delete("a"));
		Assert.assertEquals(0, underTest.size());

		underTest.add("a");
		underTest.add("b");
		Assert.assertTrue(underTest.delete("a"));
		Assert.assertTrue(underTest.delete("b"));
		Assert.assertEquals(0, underTest.size());

		underTest.add("a");
		underTest.add("b");
		underTest.add("c");
		Assert.assertTrue(underTest.delete("a"));
		Assert.assertTrue(underTest.delete("b"));
		Assert.assertEquals(1, underTest.size());
		Assert.assertFalse(underTest.delete("d"));
		Assert.assertEquals(1, underTest.size());
	}
	
	@Test
	public void testIterator() {

		underTest.add("a");
		underTest.add("b");
		underTest.add("c");
		
		ListIterator<String> iter = underTest.iterator();
		String values = "";
		while(iter.hasNext()) {
			values += iter.next();
		}
		Assert.assertEquals("abc", values);
	}
	
	@Test
	public void testToString() {
		
		Assert.assertEquals("[]", underTest.toString());
		
		underTest.add("a");
		Assert.assertEquals("[a]", underTest.toString());

		underTest.add("b");
		Assert.assertEquals("[a, b]", underTest.toString());
	}

}
