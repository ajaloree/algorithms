package com.anoop.algo.greedy;

import java.util.List;

import org.hamcrest.MatcherAssert;
import org.hamcrest.Matchers;
import org.junit.Test;

public class ChangeDispenserTest {
	
	@Test
	public void testDispenseChange() {
		
		ChangeDispenser dispenser = new ChangeDispenser(List.of(1, 5, 10));
		MatcherAssert.assertThat(dispenser.dispenseChange(28), Matchers.contains(10, 10, 5, 1, 1, 1));
		MatcherAssert.assertThat(dispenser.dispenseChange(13), Matchers.contains(10, 1, 1, 1));
		MatcherAssert.assertThat(dispenser.dispenseChange(2), Matchers.contains(1, 1));
		MatcherAssert.assertThat(dispenser.dispenseChange(6), Matchers.contains(5, 1));
	}

}