package com.anoop.algo.strings;

import org.junit.Assert;
import org.junit.Test;

public class StringCompressionTest {
	
	StringCompression underTest = new StringCompression();
	
	@Test
	public void testCompress() {
		Assert.assertEquals("a", underTest.compress("a"));
		Assert.assertEquals("aa", underTest.compress("aa"));
		Assert.assertEquals("a3", underTest.compress("aaa"));
		Assert.assertEquals("a1b2c3d4e5f4g3h2i1", underTest.compress("abbcccddddeeeeeffffggghhi"));
	}
}