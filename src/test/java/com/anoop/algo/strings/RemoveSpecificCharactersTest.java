package com.anoop.algo.strings;

import org.junit.Assert;
import org.junit.Test;

public class RemoveSpecificCharactersTest {
	
	RemoveSpecificCharacters underTest;
	
	@Test
	public void testRemoveFromFront() {
		underTest = new RemoveSpecificCharacters("abc");
		
		StringBuilder expected = new StringBuilder("defghijkl");
		StringBuilder received = underTest.removeFrom(new StringBuilder("abcdefghijkl"));
		
		Assert.assertEquals(0, expected.compareTo(received));
	}

	@Test
	public void testRemoveFromEnd() {
		underTest = new RemoveSpecificCharacters("jkl");
		
		StringBuilder expected = new StringBuilder("abcdefghi");
		StringBuilder received = underTest.removeFrom(new StringBuilder("abcdefghijkl"));
		
		Assert.assertEquals(0, expected.compareTo(received));
	}

	@Test
	public void testRemoveFromMiddle() {
		underTest = new RemoveSpecificCharacters("def");
		
		StringBuilder expected = new StringBuilder("abcghijkl");
		StringBuilder received = underTest.removeFrom(new StringBuilder("abcdefghijkl"));
		
		Assert.assertEquals(0, expected.compareTo(received));
	}

	@Test
	public void testRemoveAlternateChars() {
		underTest = new RemoveSpecificCharacters("bdfhjl");
		
		StringBuilder expected = new StringBuilder("acegik");
		StringBuilder received = underTest.removeFrom(new StringBuilder("abcdefghijkl"));
		
		Assert.assertEquals(0, expected.compareTo(received));
	}

}
