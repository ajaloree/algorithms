package com.anoop.algo.strings;

import org.junit.Assert;
import org.junit.Test;

public class LevenshteinWordDistanceCalculatorTest {

	LevenshteinWordDistanceCalculator underTest = new LevenshteinWordDistanceCalculator();
	
	@Test
	public void testDistance() {
		Assert.assertEquals(3, underTest.calculate("msteak", "mistake"));
	}
	
}
