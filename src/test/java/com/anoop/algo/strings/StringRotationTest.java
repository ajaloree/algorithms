package com.anoop.algo.strings;

import org.junit.Assert;
import org.junit.Test;

public class StringRotationTest {

	private StringRotation underTest = new StringRotation();
	
	@Test
	public void testIsRotation() {
		Assert.assertTrue(underTest.isRotation("firetruck","truckfire"));
		Assert.assertTrue(underTest.isRotation("ab","ba"));
		Assert.assertTrue(underTest.isRotation("abracadabra","dabraabraca"));
		
	}

	@Test
	public void testIsNotRotation() {
		Assert.assertFalse(underTest.isRotation("firetruck","trukcfire"));
		Assert.assertFalse(underTest.isRotation("ab","vb"));
		Assert.assertFalse(underTest.isRotation("abracadabra","dbaraabraca"));
		
	}

}