package com.anoop.algo.strings.search;

import org.junit.Assert;
import org.junit.Test;

public class BruteForceStringSearchTest {

	BruteForceStringSearch underTest = new BruteForceStringSearch();
	
	@Test
	public void testPatternInTheBeginning() {
		Assert.assertEquals(0, underTest.search("abc", "abcde"));
	}

	@Test
	public void testPatternInTheEnd() {
		Assert.assertEquals(2, underTest.search("cde", "abcde"));
	}

	@Test
	public void testPatternInMiddle() {
		Assert.assertEquals(1, underTest.search("bcd", "abcde"));
	}
}