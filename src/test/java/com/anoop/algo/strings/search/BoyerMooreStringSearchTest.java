package com.anoop.algo.strings.search;

import org.junit.Assert;
import org.junit.Test;

public class BoyerMooreStringSearchTest {

	BoyerMooreStringSearch underTest;
	
	@Test
	public void testPatternInTheBeginning() {
		underTest = new BoyerMooreStringSearch("abc");
		Assert.assertEquals(0, underTest.search("abcde"));
	}

	@Test
	public void testPatternInTheEnd() {
		underTest = new BoyerMooreStringSearch("cde");
		Assert.assertEquals(2, underTest.search("abcde"));
	}

	@Test
	public void testPatternInMiddle() {
		underTest = new BoyerMooreStringSearch("bcd");
		Assert.assertEquals(1, underTest.search("abcde"));
	}

	@Test
	public void testPatternNotFound() {
		underTest = new BoyerMooreStringSearch("xyz");
		Assert.assertEquals(5, underTest.search("abcde"));
	}

}
