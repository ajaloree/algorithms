package com.anoop.algo.strings;

import org.junit.Assert;
import org.junit.Test;

public class OneEditAwayTest {

	OneEditAway underTest = new OneEditAway();
	
	@Test
	public void testOneEditReplaceAway() {
		Assert.assertTrue(underTest.checkIfOneAway("pale", "bale"));
		Assert.assertTrue(underTest.checkIfOneAway("pale", "pace"));
		Assert.assertFalse(underTest.checkIfOneAway("pale", "ball"));
	}

	@Test
	public void testOneEditInsertAway() {
		Assert.assertTrue(underTest.checkIfOneAway("pale", "ale"));
		Assert.assertTrue(underTest.checkIfOneAway("pale", "pal"));
		Assert.assertTrue(underTest.checkIfOneAway("balloon", "baloon"));
		Assert.assertFalse(underTest.checkIfOneAway("balloon", "baloons"));
	}

	@Test
	public void testNotOneEditInsertAway() {
		Assert.assertFalse(underTest.checkIfOneAway("richa", "ric"));
	}
}