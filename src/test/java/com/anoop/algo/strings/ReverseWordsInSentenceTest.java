package com.anoop.algo.strings;

import org.junit.Assert;
import org.junit.Test;

public class ReverseWordsInSentenceTest {
	
	ReverseWordsInSentence underTest = new ReverseWordsInSentence();
	
	@Test
	public void testSingleWord() {
		Assert.assertEquals("victory", underTest.reverseWordsUsingBuffer("victory"));
	}

	@Test
	public void testTwoWords() {
		Assert.assertEquals("within victory", underTest.reverseWordsUsingBuffer("victory within"));
	}

	@Test
	public void testThreeWords() {
		Assert.assertEquals("sight! within victory", underTest.reverseWordsUsingBuffer("victory within sight!"));
	}

}
