package com.anoop.algo.kmeans;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;

public class KMeansTest {
	
	KMeans<DataPoint> underTest;
	
	@Test
	public void testKmeansForFour2DPoints() {
	
		DataPoint point1 = new DataPoint(List.of(2.0, 1.0)); 
		DataPoint point2 = new DataPoint(List.of(2.0, 2.0)); 
		DataPoint point3 = new DataPoint(List.of(5.0, 1.5)); 
		DataPoint point4 = new DataPoint(List.of(5.0, 2.5)); 
		
		underTest = new KMeans<>(2, List.of(point1, point2, point3, point4));
		
		List<KMeans<DataPoint>.Cluster> testClusters = underTest.run(100);
		
		System.out.println(testClusters);
		
		Assert.assertTrue(testClusters.size() > 1);
		
	}
	

}
