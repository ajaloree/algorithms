package com.anoop.ds.graphs;

public class DepthFirstPaths {
	
	private Integer s;
	private Integer[] edgeTo;
	private boolean[] marked;
	private Integer count = new Integer(0);
	
	public DepthFirstPaths(Graph g, Integer s) {
		this.s = s;
		marked = new boolean[g.getNumVertices()];
		edgeTo = new Integer[g.getNumVertices()];
		traverseDepthFirst(g, s);
	}
	
	private void traverseDepthFirst(Graph g, Integer v) {
		System.out.println("Vertex "+v+" visited.");
		marked[v] = true;
		count++;
		for(Integer w : g.adjacentNodes(v)) {
			if(!marked[w]) {
				edgeTo[w] = v;
				traverseDepthFirst(g, w);
			}
		}
	}
	
	public boolean hasPath(Integer w) {
		return marked[w];
	}
	
	public String printPathFrom(Integer w) {
		
		String ret = "";
		if(hasPath(w)) {
			for(int x = w; x != s; x = edgeTo[x]) {
				ret += (x + "->");
			}
			ret += s;
		} else {
			ret = "No path exists.";
		}
		return ret;
	}

}