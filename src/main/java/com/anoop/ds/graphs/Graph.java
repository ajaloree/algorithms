package com.anoop.ds.graphs;

import java.util.ArrayList;
import java.util.List;

import com.anoop.In;

public class Graph {

	private Integer numVertices = new Integer(0);
	private Integer numEdges = new Integer(0);
	private List<Integer>[] adjList;
	
	@SuppressWarnings("unchecked")
	public Graph(Integer numVertices) {
		this.numVertices = numVertices;
		adjList = (List<Integer>[])new List[numVertices];
		for(int i=0; i < numVertices; i++) {
			adjList[i] = new ArrayList<>();
		}
	}

	public Graph(In in) {
		this(in.readInt());
		int edges = in.readInt();
		for(int e = 0; e < edges; e++) {
			addEdge(in.readInt(), in.readInt());
		}
	}
	
	public Integer getNumVertices() {
		return numVertices;
	}

	public Integer getNumEdges() {
		return numEdges;
	}

	public void addEdge(Integer v, Integer w) {
		adjList[v].add(w);
		adjList[w].add(v);
		numEdges++;
	}
	
	public List<Integer> adjacentNodes(Integer v) {
		return adjList[v];
	}
	
	public Integer degree(Integer v) {
		return adjList[v].size();
	}
	
	public Integer maxDegree() {
		Integer max = 0;
		for(List<Integer> ls : adjList) {
			if(ls.size() > max) max = ls.size();
		}
		return max;
	}
	
	public Integer numSelfLoops() {
		Integer loopCount = 0;
		for(int v = 0; v < numVertices; v++) {
			if(adjList[v].contains(v)) loopCount++;
		}
		return loopCount / 2;
	}
	
	public String toString() {
		String ret = "Vertices: "+numVertices+", Edges: "+numEdges+"\n";
		for(int v = 0; v < numVertices; v++) {
			ret += "Vertex: "+v+" -> "+adjList[v]+"\n";
		}
		return ret;
	}
	
}
