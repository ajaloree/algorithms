package com.anoop.ds.graphs;

public class ConnectedComponents {
	
	private boolean[] marked;
	private Integer[] id;
	private Integer count = new Integer(0);
	
	public ConnectedComponents(Graph g) {
		this.marked = new boolean[g.getNumEdges()];
		this.id = new Integer[g.getNumEdges()];
		for(Integer v = 0; v < g.getNumVertices(); v++) {
			if(!marked[v]) {
				traverseDepthFirst(g, v);
				count++;
			}
		}
	}

	private void traverseDepthFirst(Graph g, Integer v) {
		marked[v] = true;
		id[v] = count;
		for(Integer w : g.adjacentNodes(v)) {
			if(!marked[w]) {
				traverseDepthFirst(g, w);
			}
		}
	}
	
	public Integer getCount() {
		return count;
	}
	
	public boolean connected(Integer v, Integer w) {
		return id[v] == id[w];
	}
	
	public Integer id(Integer v) {
		return id[v];
	}
	
}