package com.anoop.ds.graphs;

public class DepthFirstSearch {
	
	private Integer count = new Integer(0);
	private boolean[] marked;
	
	public DepthFirstSearch(Graph g, Integer s) {
		marked = new boolean[g.getNumVertices()];
		traverseDepthFirst(g, s);
	}
	
	private void traverseDepthFirst(Graph g, Integer v) {
		System.out.println("Vertex "+v+" visited.");
		marked[v] = true;
		count++;
		for(Integer w : g.adjacentNodes(v)) {
			if(!marked[w]) {
				traverseDepthFirst(g, w);
			}
		}
	}
	
	public boolean isConnected(Integer w) {
		return marked[w];
	}
	
	public Integer nodesVisited() {
		return count;
	}

}