package com.anoop.ds.graphs;

public class CycleDetection {
	
	private boolean[] marked;
	
	public CycleDetection(Graph g) {
		this.marked = new boolean[g.getNumEdges()];
		for(Integer v = 0; v < g.getNumVertices(); v++) {
			if(!marked[v]) {
				traverseDepthFirst(g, v);
			}
		}
	}

	private void traverseDepthFirst(Graph g, Integer v) {
		marked[v] = true;
		for(Integer w : g.adjacentNodes(v)) {
			if(!marked[w]) {
				traverseDepthFirst(g, w);
			}
		}
	}
	
}