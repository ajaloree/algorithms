package com.anoop.ds.graphs;

import java.util.LinkedList;
import java.util.Queue;

public class BreadthFirstPaths {
	
	private Integer s;
	private boolean[] marked;
	private Integer[] edgeTo;
	
	public BreadthFirstPaths(Graph g, Integer s) {
		this.s = s;
		this.marked = new boolean[g.getNumVertices()];
		this.edgeTo = new Integer[g.getNumVertices()];
		traverseBreadthFirst(g, s);
	}
	
	private void traverseBreadthFirst(Graph g, Integer v) {
		Queue<Integer> queue = new LinkedList<>();
		marked[v] = true;
		queue.add(v);
		while(!queue.isEmpty()) {
			Integer x = queue.remove();
			for(Integer neighbour : g.adjacentNodes(x)) {
				if(!marked[neighbour]) {
					marked[neighbour] = true;
					edgeTo[neighbour] = x;
					queue.add(neighbour);
				}
			}
		}
	}
	
	public boolean hasPath(Integer w) {
		return marked[w];
	}
	
	public String printPathFrom(Integer w) {
		
		String ret = "";
		if(hasPath(w)) {
			for(int x = w; x != s; x = edgeTo[x]) {
				ret += (x + "->");
			}
			ret += s;
		} else {
			ret = "No path exists.";
		}
		return ret;
	}

}