package com.anoop.ds.graphs;

import java.util.LinkedList;
import java.util.Queue;

public class BreadthFirstSearch {
	
	private Integer count = new Integer(0);
	private boolean[] marked;
	
	public BreadthFirstSearch(Graph g, Integer s) {
		marked = new boolean[g.getNumVertices()];
		traverseBreadthFirst(g, s);
	}
	
	private void traverseBreadthFirst(Graph g, Integer v) {
		Queue<Integer> queue = new LinkedList<>();
		queue.add(v);
		while(!queue.isEmpty()) {
			Integer x = queue.remove();
			if(!marked[x]) {
				System.out.println("Vertex "+x+" visited.");
				marked[x] = true;
				count++;
				queue.addAll(g.adjacentNodes(x));
			}
		}
	}
	
	public boolean isConnected(Integer w) {
		return marked[w];
	}
	
	public Integer nodesVisited() {
		return count;
	}

}