package com.anoop.ds.list2;

import java.util.ListIterator;
import java.util.NoSuchElementException;

public class DoublyLinkedList<T> {
	
	private int n;		// number of elements in the list
	private Node pre;   // sentinel before first item
	private Node post;  // sentinel after last item
	
	public DoublyLinkedList() {
		pre = new Node(null);
		post = new Node(null);
		pre.next = post;
		post.prev = pre;
	}
	
	
	private class Node {
		private T item;
		private Node next;
		private Node prev;
		
		public Node(T item) {
			this.item = item;
		}
	}
	
	public boolean isEmpty() {
		return (n == 0);
	}
	
	public int size() {
		return n;
	}
	
	public void add(T item) {
		Node last = post.prev;
		Node x = new Node(item);
		x.next = post;
		x.prev = last;
		post.prev = x;
		last.next = x;
		n++;
	}

	public T get(int index) {
		checkOutOfBounds(index);
		return getNodeAtIndex(index).item;
	}

	public void set(int index, T item) {
		checkOutOfBounds(index);
		Node old = getNodeAtIndex(index);
		Node x = new Node(item);
		old.prev.next = x;
		x.prev = old.prev;
		x.next = old;
		old.prev = x;
		n++;
	}
	
	public int indexOf(T item) {

		Node node = pre;
		for(int i = 0; i <= n-1; i++) {
			node = node.next;
			if(node.item.equals(item)) {
				return i;
			}
		}
		return -1;
	}

	public boolean contains(T item) {
		return indexOf(item) != -1;
	}
	
	public T delete(int index) {

		checkOutOfBounds(index);
		Node x = getNodeAtIndex(index);
		x.prev.next = x.next;
		x.next.prev = x.prev;
		n--;
		return x.item;
	}

	public boolean delete(T item) {
		
		int idx = indexOf(item);
		if(idx == -1) {
			return false;
		} else {
			delete(idx);
			return true;
		}
	}

 	private Node getNodeAtIndex(int index) {
		
		Node node = pre;
		for(int i = 0; i <= index; i++) {
			node = node.next;
		}
		return node;
	}
	
	private void checkOutOfBounds(int index) {
		if(isOutOfBounds(index)) {
			throw new IndexOutOfBoundsException();
		}
	}

	private boolean isOutOfBounds(int index) {
		return index < 0 || index >= n;
	}
	
	public ListIterator<T> iterator()  { 
		return new DoublyLinkedListIterator(); 
	}
	
	@Override
	public String toString() {
		if(n == 0) return "[]";
		StringBuilder ret = new StringBuilder();
		ret.append("[");
		Node node = pre;
		for(int i = 0; i <= n-1; i++) {
			node = node.next;
			ret.append(node.item);
			if(i != n-1) {
				ret.append(", ");
			}
		}
		ret.append("]");
		return ret.toString();
	}
	
	private class DoublyLinkedListIterator implements ListIterator<T> {
		
		private Node current = pre.next; 	// the node that is returned by next()
		private Node lastAccessed = null;	// the last node to be returned by prev() or next()
											// reset to null upon intervening remove() or add()
		private int index = 0;
		
		@Override
		public boolean hasNext() { return index < n; }

		@Override
		public boolean hasPrevious() { return index > 0; }

		@Override
		public int nextIndex() { return index + 1; }

		@Override
		public int previousIndex() { return index - 1; }

		@Override
		public T next() {
			if(!hasNext()) throw new NoSuchElementException();
			lastAccessed = current;
			T item = current.item;
			current = current.next;
			index++;
			return item;
		}

		@Override
		public T previous() {
			if(!hasPrevious()) throw new NoSuchElementException();
			current = current.prev;
			index--;
			lastAccessed = current;
			return current.item;
		}

        // remove the element that was last accessed by next() or previous()
        // condition: no calls to remove() or add() after last call to next() or previous()
		@Override
		public void remove() {
			if (lastAccessed == null) throw new IllegalStateException();
			Node x = lastAccessed.prev;
			Node y = lastAccessed.next;
			x.next = y;
			y.prev = x;
			n--;
			if (current == lastAccessed)
				current = y;
			else
				index--;
			lastAccessed = null;	
		}

        // replace the item of the element that was last accessed by next() or previous()
        // condition: no calls to remove() or add() after last call to next() or previous()
		@Override
		public void set(T item) {
            if (lastAccessed == null) throw new IllegalStateException();
            lastAccessed.item = item;			
		}

        // add element to list 
		@Override
		public void add(T item) {
            Node x = current.prev;
            Node y = new Node(item);
            Node z = current;
            x.next = y;
            y.next = z;
            z.prev = y;
            y.prev = x;
            n++;
            index++;
            lastAccessed = null;			
		}
	}

}
