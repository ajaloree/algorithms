package com.anoop.ds.list;

public interface MyList<V> {
	
		public void insert(int index, V value);
		
		public void add(V value);
		
		public V get(int index);
		
		public V set(int index, V value);
		
		public int indexOf(V value);
		
		public boolean contains(V value);
		
		public V delete(int index);
		
		public boolean delete(V value);
		
		public int size();
		
		public boolean isEmpty();
		
		public void clear();
		
}