package com.anoop.ds.list;

import java.util.Iterator;

/**
 * A list implementation backed by an array.
 * 
 * @author ajaloree
 *
 */
public class ArrayList implements MyList {

	private static final int DEFAULT_INITIAL_CAPACITY = 16;
	
	private final int initialCapacity;
	private Object[] array;
	private int size;
	
	public ArrayList() {
		this(DEFAULT_INITIAL_CAPACITY);
	}
	
	public ArrayList(int initialCapacity) {
		this.initialCapacity = initialCapacity;
		clear();
	}
	
	@Override
	public void clear() {
		array = new Object[initialCapacity];
		size = 0;
	}
	
	@Override
	public void insert(int index, Object value) {
		if(index < 0 || index > size) {
			throw new IllegalArgumentException();
		}
		ensureCapacity(size+1);
		System.arraycopy(array, index, array, index+1, size-index);
		array[index] = value;
		++size;
	}
	
	@Override
	public void add(Object value) {
		insert(size, value);
	}
	
	@Override
	public Object get(int index) {
		checkOutOfBounds(index);
		return array[index];
	}
	
	@Override
	public Object set(int index, Object value) {
		checkOutOfBounds(index);
		Object oldValue = array[index];
		array[index] = value;
		return oldValue;
	}
	
	@Override
	public int indexOf(Object value) {
		for(int i = 0; i < size; i++) {
			if(value.equals(array[i])) {
				return i;
			}
		}
		return -1;
	}
	
	@Override
	public boolean contains(Object value) {
		return indexOf(value) != -1;
	}
	
	@Override
	public Object delete(int index) {
		checkOutOfBounds(index);
		Object value = array[index];
		int copyFromIndex = index+1;
		if(copyFromIndex < size) {
			System.arraycopy(array, copyFromIndex, array, index, size - copyFromIndex);
		}
		array[--size] = null;
		return value;
	}
	
	@Override
	public boolean delete(Object value) {
		int index = indexOf(value);
		if(index != -1) {
			delete(index);
			return true;
		}
		return false;
	}
	
	@Override
	public int size() {
		return size;
	}
	
	@Override
	public boolean isEmpty() {
		return size() == 0;
	}
	
	private void ensureCapacity(int capacity) {
		if(array.length < capacity) {
			Object[] copy = new Object[capacity + capacity/2];
			System.arraycopy(array, 0, copy, 0, size);
			array = copy;
		}
	}

	private void checkOutOfBounds(int index) {
		if(isOutOfBounds(index)) {
			throw new IndexOutOfBoundsException();
		}
	}
	
	private boolean isOutOfBounds(int index) {
		return (index < 0) || (index >= size);
	}
}
