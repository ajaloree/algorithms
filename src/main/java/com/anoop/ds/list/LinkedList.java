package com.anoop.ds.list;

import com.anoop.ds.list2.DoublyLinkedList;

/**
 * 
 * Favour alternate implementation which is: {@link DoublyLinkedList}
 * 
 * @author ajaloree
 *
 * @param <V>
 */
public class LinkedList<V> implements MyList<V> {

	private final Element<V> headAndTail = new Element(null);
	private int size;
	
	public LinkedList() {
		clear();
	}
	
	@Override
	public void insert(int index, V value) {
		if(index < 0 || index > size) {
			throw new IndexOutOfBoundsException();
		}
		
		Element<V> e = new Element<V>(value);
		e.attachBefore(getElement(index));
		size++;
	}

	@Override
	public void add(V value) {
		insert(size, value);
	}

	@Override
	public V get(int index) {
		checkOutOfBounds(index);
		return getElement(index).value;
	}

	@Override
	public V set(int index, V value) {
		checkOutOfBounds(index);
		Element<V> e = getElement(index);
		V oldValue = e.getValue();
		e.setValue(value);
		return oldValue;
	}

	@Override
	public int indexOf(V value) {
		
		int index = 0;
		for(Element<V> e = headAndTail.getNext();
			e!= headAndTail;
			e = e.getNext()) {
			if(value.equals(e.getValue())) {
				return index;
			}
		}
		index++;
		return -1;
	}

	@Override
	public boolean contains(V value) {
		return indexOf(value) != -1;
	}

	@Override
	public V delete(int index) {
		checkOutOfBounds(index);
		Element<V> e = getElement(index);
		e.detach();
		size--;
		return e.getValue();
	}

	@Override
	public boolean delete(V value) {
		for(Element<V> e = headAndTail.getNext();
				       e!= headAndTail;
				       e = e.getNext()) {
			
			if(value.equals(e.getValue())) {
				e.detach();
				size--;
				return true;
			}
		}
		return false;
	}

	@Override
	public int size() {
		return size;
	}

	@Override
	public boolean isEmpty() {
		return size == 0;
	}
	
	@Override
	public void clear() {
		headAndTail.setPrevious(headAndTail);
		headAndTail.setNext(headAndTail);
		size = 0;
	}

	private Element<V> getElement(int index) {
		Element<V> e = headAndTail.next;
		for(int i = index; i > 0; --i) {
			e = e.getNext();
		}
		return e;
	}
	
	private void checkOutOfBounds(int index) {
		if(isOutOfBounds(index)) {
			throw new IndexOutOfBoundsException();
		}
	}
	
	private boolean isOutOfBounds(int index) {
		return index < 0 || index >= size;
	}
	
	private static final class Element<V> {
		
		private V value;
		private Element<V> previous;
		private Element<V> next;
		
		public Element(V value) {
			setValue(value);
		}

		public V getValue() {
			return value;
		}

		public void setValue(V value) {
			this.value = value;
		}

		public Element<V> getPrevious() {
			return previous;
		}

		public void setPrevious(Element<V> previous) {
			this.previous = previous;
		}

		public Element<V> getNext() {
			return next;
		}

		public void setNext(Element<V> next) {
			this.next = next;
		}
		
		public void attachBefore(Element<V> next) {
			
			Element<V> previous = next.getPrevious();
			
			setNext(next);
			setPrevious(previous);
			
			next.setPrevious(this);
			previous.setNext(this);
		}
		
		public void detach() {
			previous.setNext(next);
			next.setPrevious(previous);
		}
	}
}