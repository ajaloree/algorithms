package com.anoop.ds.graphs2;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.Stack;
import java.util.function.Predicate;

public class UnweightedGraph<V> extends Graph<V, Edge> {

	public UnweightedGraph(List<V> vertices) {
		super(vertices);
	}

	// This is an undirected graph, so we always add edges in both directions
	public void addEdge(Edge edge) {
		edges.get(edge.u).add(edge);
		edges.get(edge.v).add(edge.reversed());
	}
	    
	// Add an edge using vertex indices (convenience method)
	public void addEdge(int u, int v) { 
		addEdge(new Edge(u, v));
	}
	
	// Add an edge by looking up vertex indices (convenience method)
	public void addEdge(V first, V second) {
		addEdge(new Edge(indexOf(first), indexOf(second)));
	}
	
	// Breadth first search
	public List<V> bfsPath(V initial, Predicate<V> goalTest) {
		
		Queue<V> queue = new LinkedList<>();
		Set<V> visited = new HashSet<>();
		Map<V,V> edgeMap = new HashMap<>();
		
		queue.add(initial);
		visited.add(initial);
		
		while(!queue.isEmpty()) {
			
			V currentNode = queue.poll();
			
			if(goalTest.test(currentNode)) {
				return generateNodePath(currentNode, edgeMap);
			} else {
				for(V neighbour : neighborsOf(currentNode)) {
					if(!visited.contains(neighbour)) {
						visited.add(neighbour);
						queue.add(neighbour);
						edgeMap.put(neighbour, currentNode);
					}
				}
			}
		}
		return null;
	}
	
	private List<V> generateNodePath(V end, Map<V,V> edgeMap) {
		Stack<V> stack = new Stack<>();
		List<V> path = new ArrayList<>();
		V node = end;
		while(node != null) {
			stack.push(node);
			node = edgeMap.get(node);
		}
		while(!stack.isEmpty()) path.add(stack.pop());
		return path;
	}
}