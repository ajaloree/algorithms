package com.anoop.ds.graphs2;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.PriorityQueue;
import java.util.function.IntConsumer;

public class WeightedGraph<V> extends Graph<V, WeightedEdge> {

	public WeightedGraph(List<V> vertices) {
		super(vertices);
	}

	// This is an undirected graph, so we always add // edges in both directions
	public void addEdge(WeightedEdge edge) {
		edges.get(edge.u).add(edge);
		edges.get(edge.v).add(edge.reversed());
	}

	public void addEdge(int u, int v, float weight) {
		addEdge(new WeightedEdge(u, v, weight));
	}

	public void addEdge(V first, V second, float weight) {
		addEdge(indexOf(first), indexOf(second), weight);
	}

	public static double totalWeight(List<WeightedEdge> path) { 
		return path.stream().mapToDouble(we -> we.weight).sum();
	}

	/**
	 * Prim's algorithm to find minimum spanning tree.
	 * A minimum spanning tree is a tree that connects every vertex in a weighted graph
	 * with the minimum total weight. 
	 */
	public List<WeightedEdge> mst(int start) {
		
		LinkedList<WeightedEdge> result = new LinkedList<>(); // mst 
		
		if (start < 0 || start > (getVertexCount() - 1)) {
			return result; 
		}
		
		PriorityQueue<WeightedEdge> pq = new PriorityQueue<>(); 
		boolean[] visited = new boolean[getVertexCount()]; // seen it
	    
		// this is like a "visit" inner function
		IntConsumer visit = index -> {
			visited[index] = true; // mark as visited 
			for (WeightedEdge edge : edgesOf(index)) {
				// add all edges coming from here to pq 
				if (!visited[edge.v]) {
					pq.offer(edge);
				}
			}
		};
		
		visit.accept(start); // the start vertex is where we begin 
		
		while (!pq.isEmpty()) { // keep going while there are edges
			WeightedEdge edge = pq.poll(); 
			if (visited[edge.v]) {
				continue; // don't ever revisit 
			}
            // this is the current smallest, so add it to solution
			result.add(edge);
			visit.accept(edge.v); // visit where this connects 
		}
		return result; 
	}
	
	public void printWeightedPath(List<WeightedEdge> wp) { 
		for (WeightedEdge edge : wp) {
			System.out.println(vertexAt(edge.u) + " "+ edge.weight + "> " + vertexAt(edge.v)); 
		}
		System.out.println("Total Weight: " + totalWeight(wp)); 
	}
	
	// Make it easy to pretty-print a Graph
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < getVertexCount(); i++) {
			sb.append(vertexAt(i));
			sb.append(" -> ");
			sb.append(Arrays.toString(edgesOf(i).stream().map(we -> "(" + vertexAt(we.v) + ", " + we.weight + ")").toArray()));
			sb.append(System.lineSeparator());
		}
		return sb.toString();
	}
}
