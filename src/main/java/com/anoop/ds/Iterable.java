package com.anoop.ds;

public interface Iterable<V> {
	
	public Iterator<V> iterator();

}
