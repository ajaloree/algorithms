package com.anoop.ds.trees;

import java.util.ArrayList;
import java.util.List;

public class SortedArrayToBST {
	
	public BinaryTreeNode convertToNode(List<Integer> input) {
		
		ArraySplits splits = splitInMiddleLeftAndRightParts(input);
		
		BinaryTreeNode node = new BinaryTreeNode(splits.middle, Integer.toString(splits.middle));

		if(!splits.left.isEmpty()) {
			node.left = convertToNode(splits.left);
		}
		
		if(!splits.right.isEmpty()) {
			node.right = convertToNode(splits.right);
		}
		
		return node;
	}
	
	public ArraySplits splitInMiddleLeftAndRightParts(List<Integer> input) {
		
		ArraySplits ret = new ArraySplits();
		if(input.size() == 1) {
			ret.middle = input.get(0);
		} else if(input.size() == 2){
			ret.left.add(input.get(0));
			ret.middle = input.get(1);
		} else {
			int mid = input.size() / 2;
			for(int i=0; i<mid; i++) {
				ret.left.add(input.get(i));
			}
			for(int i=mid+1; i < input.size(); i++) {
				ret.right.add(input.get(i));
			}
			ret.middle = input.get(mid);
		}
				
		return ret;
	}
	
	protected class ArraySplits {
		public List<Integer> left = new ArrayList<>();
		public Integer middle = null;
		public List<Integer> right = new ArrayList<>();
		
		@Override
		public String toString() {
			return "Left: "+left+", Middle:"+middle+", Right:"+right;
		}
	}
}
