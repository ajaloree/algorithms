package com.anoop.ds.trees;

import java.util.LinkedList;
import java.util.Queue;

public class BinarySearchTree<K, V> {
	
	private Node root = null;
	
	protected class Node {
		
		public K key;
		public V value;
		public Node left = null;
		public Node right = null;
		
		public Node(K key, V value) {
			this.key = key;
			this.value    = value;
		}
		
		@Override
		public String toString() {
			return key+"("+value+")";

		}
	}

	public void put(K key, V value) {
		if(root == null) {
			root = new Node(key, value);
		} else {
			put(key, value, root);
		}
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private void put(K key, V value, Node node) {
		
		if(((Comparable)key).compareTo(node.key) == 0) {
			node.key = key;
			node.value = value;
		} else if(((Comparable)key).compareTo(node.key) < 0) {
			if(node.left == null) {
				node.left = new Node(key, value);
			} else {
				put(key, value, node.left);
			}
		} else {
			if(node.right == null) {
				node.right = new Node(key, value);
			} else {
				put(key, value, node.right);
			}
		}
	}
	
	public V get(K key) {
		return get(key, root);
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private V get(K key, Node node) {
		if(node == null) {
			return null;
		} else if(((Comparable)key).compareTo(node.key) == 0) {
			return node.value;
		} else if(((Comparable)key).compareTo(node.key) < 0) {
			return get(key, node.left);
		} else {
			return get(key, node.right);
		}
	}

	/**
	 * In-order traversal means to visit the left branch, then the root node, and then finally the right branch.
	 */
	public void inOrderTraversal() {
		System.out.println("*** In-Order Traversal Begin ***");
		inOrderTraversal(root);
		System.out.println("*** In-Order Traversal End ***");
	}
	
	private void inOrderTraversal(Node node) {
		if(node != null) {
			inOrderTraversal(node.left);
			System.out.println(node);
			inOrderTraversal(node.right);
		}
	}

	/**
	 * Pre-order traversal means to visit the root node, and then child nodes.
	 * Root is always the first node in pre-order traversal.
	 */
	public void preOrderTraversal() {
		System.out.println("*** Pre-Order Traversal Begin ***");
		preOrderTraversal(root);
		System.out.println("*** Pre-Order Traversal End ***");
	}
	
	private void preOrderTraversal(Node node) {
		if(node != null) {
			System.out.println(node);
			preOrderTraversal(node.left);
			preOrderTraversal(node.right);
		}
	}

	/**
	 * Post-order traversal means to visit the root node only after visiting the child nodes.
	 * Root is always the last node in post-order traversal.
	 */
	public void postOrderTraversal() {
		System.out.println("*** Post-Order Traversal Begin ***");
		postOrderTraversal(root);
		System.out.println("*** Post-Order Traversal End ***");
	}
	
	private void postOrderTraversal(Node node) {
		if(node != null) {
			postOrderTraversal(node.left);
			postOrderTraversal(node.right);
			System.out.println(node);
		}
	}

	public void breadthFirstTraversal() {
		
		Queue<Node> queue = new LinkedList<>();
		queue.add(root);
		while(!queue.isEmpty()) {
			Node node = queue.remove();
			if(node != null) {
				System.out.println(node);
				queue.add(node.left);
				queue.add(node.right);
			}
		}
	}
	
	public void printMin() {
		System.out.println("Minimum Value: "+printMin(root));
	}
	
	private String printMin(Node node) {
		if(node != null) {
			if(node.left != null) {
				return printMin(node.left);
			} else {
				return node.toString();
			}
		}
		return null;
	}

	public void printMax() {
		System.out.println("Maximum Value: "+printMax(root));
	}
	
	private String printMax(Node node) {
		if(node != null) {
			if(node.right != null) {
				return printMax(node.right);
			} else {
				return node.toString();
			}
		}
		return null;
	}
}
