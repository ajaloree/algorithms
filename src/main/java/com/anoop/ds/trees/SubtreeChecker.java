package com.anoop.ds.trees;

import java.util.LinkedList;
import java.util.Queue;

public class SubtreeChecker {
	
	/**
	 * Compares if one binary tree is a sub-tree of the other.
	 * @param rootT1: Small binary tree that is a potential subtree within the larger tree rootT2
	 * @param rootT2: Large binary tree which might potentially have rootT1 as a subset
	 * @return
	 */
	public boolean isSubtree(BinaryTreeNode rootT1, BinaryTreeNode rootT2) {
		
		BinaryTreeNode subRootT2 = getSubRootT2fromT1(rootT1, rootT2);
		if(subRootT2 == null) {
			return false;
		} else {
			return matchNodes(rootT1, subRootT2);
		}
	}
	
	/**
	 * Algorithm similar to breadth first that searches within rootT2 
	 * and returns the node where both three match first.
	 * @param rootT1
	 * @param rootT2
	 * @return
	 */
	private BinaryTreeNode getSubRootT2fromT1(BinaryTreeNode rootT1, BinaryTreeNode rootT2) {
		
		if(rootT2.key == rootT1.key) {
			return rootT2;
		} else {
			Queue<BinaryTreeNode> queue = new LinkedList<>();
			queue.add(rootT2);
			while(!queue.isEmpty()) {
				BinaryTreeNode nodeFromT2 = queue.remove();
				if(nodeFromT2.key == rootT1.key) {
					return nodeFromT2;
				} else {
					if(nodeFromT2.left  != null) queue.add(nodeFromT2.left);
					if(nodeFromT2.right != null) queue.add(nodeFromT2.right);
				}
			}
		}
		return null;
	}

	/**
	 * Performs a node by node comparison on both the trees.
	 * @param rootT1
	 * @param rootT2
	 * @return
	 */
	private boolean matchNodes(BinaryTreeNode rootT1, BinaryTreeNode rootT2) {
		
		if(rootT1 == null && rootT2 == null) {
			return true;
		} else if(rootT1 == null ^ rootT2 == null) {
			return false;
		} else if(rootT1.key == null && rootT2.key == null) {
			return true;
		} else if (rootT1.key == null ^ rootT2.key == null) {
			return false;
		} else {
			return (rootT2.key == rootT1.key) && 
				   matchNodes(rootT2.left, rootT1.left) && 
				   matchNodes(rootT2.right, rootT1.right); 
		}
	}
	
}