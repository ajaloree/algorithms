package com.anoop.ds.trees;

public class BinaryTreeNode {

	public Integer key;
	public String value;
	public BinaryTreeNode left = null;
	public BinaryTreeNode right = null;

	public BinaryTreeNode(Integer key, String value) {
		this.key = key;
		this.value = value;
	}

	@Override
	public String toString() {
		return key+"("+value+")";
	}

}