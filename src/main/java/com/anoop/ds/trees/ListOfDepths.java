package com.anoop.ds.trees;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ListOfDepths {
	
	public List<List<BinaryTreeNode>>  listChildren(BinaryTreeNode root) {
		return listChildren(Stream.of(root).collect(Collectors.toList()));
	}
	
	private List<List<BinaryTreeNode>> listChildren(List<BinaryTreeNode> nodes) {
				
		List<List<BinaryTreeNode>> ret = new ArrayList<>();
		Queue<List<BinaryTreeNode>> queue = new LinkedList<>();
		
		queue.add(nodes);
		ret.add(nodes);
		
		while(!queue.isEmpty()) {
			List<BinaryTreeNode> children = getChildNodes(queue.remove());
			if(!children.isEmpty()) {
				ret.add(children);
				queue.add(children);
			}
		}
		
		return ret;
	}

	private List<BinaryTreeNode> getChildNodes(List<BinaryTreeNode> nodes) {
		
		List<BinaryTreeNode> children = new ArrayList<>();
		for(BinaryTreeNode node : nodes) {
			if(node.left != null) children.add(node.left);
			if(node.right != null) children.add(node.right);
		}
		return children;
	}
}