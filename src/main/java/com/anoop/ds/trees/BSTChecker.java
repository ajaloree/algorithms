package com.anoop.ds.trees;

public class BSTChecker {

	public boolean isBST(BinaryTreeNode root) {
		if(root == null) {
			return false;
		} else {
			return isBSTNode(root);
		}
	}
	
	private boolean isBSTNode(BinaryTreeNode node) {
		
		if(node == null) {
			return true;
		}
		if((node.left != null) && (node.left.key > node.key)) {
			return false;
		}
		if((node.right != null) && (node.right.key < node.key)) {
			return false;
		}
		return isBSTNode(node.left) && isBSTNode(node.right);
	}
}