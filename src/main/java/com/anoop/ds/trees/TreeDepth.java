package com.anoop.ds.trees;

public class TreeDepth {
	
	public int depth(BinaryTreeNode root) {
		
		if(root.left == null && root.right == null) {
			return 0;
		} else {
			return Math.max(depth(root.left), depth(root.right))+1;
		}
	}

}