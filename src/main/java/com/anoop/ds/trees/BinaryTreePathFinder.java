package com.anoop.ds.trees;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.stream.Collectors;

public class BinaryTreePathFinder {
	
	private final Node root;
	private final Map<String, String> parentMap;
	private final List<String> leaves;
	
	public BinaryTreePathFinder(Node root) {
		this.root = root;
		this.parentMap = new HashMap<>();
		this.leaves = new ArrayList<>();
		populateParentMap();
	}
	
	private void populateParentMap() {
		
		if(root != null) {
			parentMap.put(root.data, null);
			Queue<Node> queue = new LinkedList<>();
			queue.add(root);
			while(!queue.isEmpty()) {
				
				Node node = queue.remove();

				if(isLeafNode(node)) {
					leaves.add(node.data);
				}
				if(node.left != null) {
					queue.add(node.left);
					parentMap.put(node.left.data, node.data);
				}
				if(node.right != null) {
					queue.add(node.right);
					parentMap.put(node.right.data, node.data);
				}
				
			}
		}
	}

	private boolean isLeafNode(Node node) {
		return (node != null) && (node.left == null) && (node.right == null);
	}

	public String reverse(String s) {
		
		StringBuilder rev = new StringBuilder();
		for(int i = s.length()-1; i >= 0; i--) {
			rev.append(s.charAt(i));
		}
		return rev.toString();
	}

	public String getPathFromRootToNode(String node) {
		
		if(!parentMap.containsKey(node)) {
			return "Node not present in tree";
		}

		Deque<String> deque = new ArrayDeque<>();
		while(node != null) {
			deque.addFirst(node);
			node = parentMap.get(node);
		}
		return deque.stream().collect(Collectors.joining());
	}
	
	public List<String> getAllPathsFromRoot() {
		List<String> paths = new ArrayList<>();
		for(String leaf : leaves) {
			paths.add(getPathFromRootToNode(leaf));
		}
		return paths;
	}
	
	public String getPathBetweenNodes(String n1, String n2) {

		if(!parentMap.containsKey(n1) || !parentMap.containsKey(n2)) {
			return "No Path Exists";
		}

		String pathFromN1toRoot = reverse(getPathFromRootToNode(n1));
		String pathFromN2toRoot = reverse(getPathFromRootToNode(n2));
		
		int idx1 = -1, idx2 = -1;
		
		for(int i = 0; i < pathFromN1toRoot.length(); i++) {
			
			int idxInPath2 = pathFromN2toRoot.indexOf(pathFromN1toRoot.charAt(i));
			if(idxInPath2 != -1) {
				idx1 = i;
				idx2 = idxInPath2;
				break;
			}
		}
		return pathFromN1toRoot.substring(0, idx1+1)+reverse(pathFromN2toRoot.substring(0, idx2));
	}
}