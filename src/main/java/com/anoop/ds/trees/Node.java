package com.anoop.ds.trees;

public class Node {
	
	public final String data;
	
	public Node(String data) {
		this.data = data;
	}
	
	public Node left;
	public Node right;

}