package com.anoop.ds.trees;

import java.util.LinkedList;
import java.util.Queue;

public class BinaryTreeTraverser {
	
	public static void traverseBreadthFirst(BinaryTreeNode node) {
		if(node.key != null) {
			Queue<BinaryTreeNode> queue = new LinkedList<>();
			queue.add(node);
			while(!queue.isEmpty()) {
				BinaryTreeNode n = queue.remove();
				System.out.println(n);
				if(n.left != null) queue.add(n.left);
				if(n.right != null) queue.add(n.right);
			}
		}
	}
}