package com.anoop.ds.trees;

public class TreeBalancedChecker {
	
	private int depth(BinaryTreeNode root) {
		
		if(root == null) {
			return 0;
		} else {
			return Math.max(depth(root.left), depth(root.right))+1;
		}
	}

	public boolean isBalanced(BinaryTreeNode root) {
		
		if(root == null) {
			return true;
		} else {
			int leftDepth = depth(root.left);
			int rightDepth = depth(root.right);
			if(Math.abs(rightDepth - leftDepth) > 1) {
				return false;
			} else {
				return isBalanced(root.left) && isBalanced(root.right);
			}
		}
	}

}