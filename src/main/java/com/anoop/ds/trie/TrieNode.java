package com.anoop.ds.trie;

import java.util.HashMap;
import java.util.Map;

public class TrieNode {

	public final Map<Character, TrieNode> children = new HashMap<>(); 
	
	@Override
	public String toString() {
		return children.toString();
	}

}