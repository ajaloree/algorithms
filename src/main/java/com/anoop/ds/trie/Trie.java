package com.anoop.ds.trie;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Trie {

	public final TrieNode root = new TrieNode();

	public TrieNode search(String word) {
		
		TrieNode currentNode = root;
		for(Character c : word.toCharArray()) {
			if(currentNode.children.containsKey(c)) {
				currentNode = currentNode.children.get(c);
			} else {
				return null;
			}
		}
		return currentNode;
	}
	
	
	public void insert(String word) {
		
		TrieNode currentNode = root;
		
		for(Character c : word.toCharArray()) {
			
			if(currentNode.children.containsKey(c)) {
				currentNode = currentNode.children.get(c);
			} else {
				TrieNode newNode = new TrieNode();
				currentNode.children.put(c, newNode);
				currentNode = newNode;
			}
		}
		currentNode.children.put('*', null);
	}
	
	public List<String> collectAllWords(TrieNode node, String word, List<String> words) {
		
		TrieNode currentNode = (node != null) ? node : root;
		
		for(Character c : currentNode.children.keySet()) {
			
			if(c.equals('*')) {
				words.add(word);
			} else {
				collectAllWords(currentNode.children.get(c), word.concat(c.toString()), words);
			}
		}
		return words;
	}
	
	public List<String> autocomplete(String word) {
		
		TrieNode node = search(word);
		if(node == null) {
			return Collections.emptyList();
		} else {
			return collectAllWords(node, word, new ArrayList<String>());
		}
	}
	
	@Override
	public String toString() {
		return root.toString();
	}
	
}