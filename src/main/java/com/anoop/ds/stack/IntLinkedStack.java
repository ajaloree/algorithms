package com.anoop.ds.stack;

public class IntLinkedStack implements IntStack {

	private Node head = null;
	private int size = 0;
	
	public class Node {
		private int data;
		private Node next;
		public Node(int data, Node next) {
			this.data = data;
			this.next = next;
		}
	}
	
	@Override
	public boolean isEmpty() {
		return (head == null);
	}

	@Override
	public int peek() {
		if(head == null) {
			throw new RuntimeException("Empty Stack - peek() not allowed!");
		}
		return head.data;
	}

	@Override
	public int pop() {
		int data = head.data;
		head = head.next;
		size--;
		return data;
	}

	@Override
	public void push(int data) {
		if(head == null) {
			head = new Node(data, null);
		} else {
			Node n = new Node(data, head);
			head = n;
		}
		size++;
	}

	@Override
	public int size() {
		return size;
	}
}