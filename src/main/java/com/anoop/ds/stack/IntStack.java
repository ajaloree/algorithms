package com.anoop.ds.stack;

public interface IntStack {

	public boolean isEmpty(); 
	public int peek();
	public int pop();
	public void push(int item); 
	public int size();
	
}
