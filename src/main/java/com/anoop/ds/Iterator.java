package com.anoop.ds;

public interface Iterator<V> {

	public void first();
	
	public void last();
	
	public boolean isDone();
	
	public void next();
	
	public void previous();
	
	public V current() throws IteratorOutOfBoundsException;
	
}
