package com.anoop.ds.map.hash_map;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.anoop.ds.map.DefaultEntry;
import com.anoop.ds.map.Map;
import com.anoop.ds.map.list_map.ListMap;

public class HashMap<K, V> implements Map<K, V> {
	
	public static final int DEFAULT_CAPACITY = 17;
	public static final float DEFAULT_LOAD_FACTOR = 0.75f;
	
	private final int initialCapacity;
	private final float loadFactor;
	
	private ListMap<K, V>[] buckets;
	private int size;
	
	public HashMap() {
		this(DEFAULT_CAPACITY,DEFAULT_LOAD_FACTOR);
	}
	
	public HashMap(int initialCapacity) {
		this(initialCapacity,DEFAULT_LOAD_FACTOR);
	}
	
	public HashMap(int initialCapacity, float loadFactor) {
		this.initialCapacity = initialCapacity;
		this.loadFactor = loadFactor;
		clear();
	}
	
	@Override
	public Iterator<DefaultEntry<K, V>> iterator() {

		List<DefaultEntry<K, V>> list = new ArrayList<>();
		for(ListMap<K,V> bucket : buckets) {
			Iterator<DefaultEntry<K, V>> iterator = bucket.iterator();
			while(iterator.hasNext()) {
				list.add(iterator.next());
			}
		}
		return list.iterator();
	}

	@Override
	public V get(K key) {
		ListMap<K, V> bucket = buckets[bucketIndexFor(key)];
		return bucket != null ? bucket.get(key) : null;
	}

	@Override
	public V set(K key, V value) {
		ListMap<K, V> bucket = bucketFor(key);
		int sizeBefore = bucket.size();
		V oldValue = bucket.set(key, value);
		if(bucket.size() > sizeBefore) {
			++size;
			maintainLoad();
		}
		return oldValue;
	}

	@Override
	public V delete(K key) {
		ListMap<K, V> bucket = buckets[bucketIndexFor(key)];
		if(bucket == null) {
			return null;
		}
		int sizeBefore = bucket.size();
		V value = bucket.delete(key);
		if(bucket.size() < sizeBefore) {
			--size;
		}
		return value;
	}

	@Override
	public boolean contains(K key) {
		ListMap<K, V> bucket = buckets[bucketIndexFor(key)];
		return bucket != null && bucket.contains(key);
	}

	@Override
	public void clear() {
		buckets = new ListMap[initialCapacity];
		size = 0;
	}

	@Override
	public int size() {
		return size;
	}

	@Override
	public boolean isEmpty() {
		return size() == 0;
	}
	
	private int bucketIndexFor(K key) {
		return Math.abs(key.hashCode() % buckets.length);
	}
	
	private ListMap<K, V> bucketFor(K key) {
		int bucketIndex = bucketIndexFor(key);
		ListMap<K, V> bucket = buckets[bucketIndex];
		if(bucket == null) {
			bucket = new ListMap<>();
			buckets[bucketIndex] = bucket;
		}
		return bucket;
	}
	
	private boolean loadFactorExceeded() {
		return size() > buckets.length * loadFactor;
	}
	
	private void resize() {
		HashMap<K, V> copy = new HashMap<>(buckets.length * 2, loadFactor);
		for(ListMap<K, V> bucket : buckets) {
			copy.addAll(bucket.iterator());
		}
		buckets = copy.buckets;
	}
	
	private void maintainLoad() {
		if(loadFactorExceeded()) {
			resize();
		}
	}
	
	private void addAll(Iterator<DefaultEntry<K, V>> iterator) {
		while(iterator.hasNext()) {
			Map.Entry<K, V> entry = iterator.next();
			set(entry.getKey(), entry.getValue());
		}
	}
}