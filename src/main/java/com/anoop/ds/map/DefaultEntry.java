package com.anoop.ds.map;

public class DefaultEntry<K,V> implements Map.Entry<K, V> {

	private final K key;
	private V value;
	
	public DefaultEntry(K key, V value) {
		if(key == null) throw new RuntimeException("Key cannot be null");
		this.key = key;
		setValue(value);
	}
	
	@Override
	public K getKey() {
		return this.key;
	}
	
	public V setValue(V value) {
		V oldValue = this.value;
		this.value = value;
		return oldValue;
	}
	
	@Override
	public V getValue() {
		return this.value;
	}
	
	@Override
	public boolean equals(Object object) {

		if(this == object) {
			return true;
		}
		
		if(object == null || getClass() != object.getClass()) {
			return false;
		}
		
		DefaultEntry<K, V> other = (DefaultEntry<K, V>) object;
		
		return this.key.equals(other.getKey()) && this.value.equals(other.getValue());
	}
	
}
