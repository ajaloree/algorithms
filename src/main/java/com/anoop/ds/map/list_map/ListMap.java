package com.anoop.ds.map.list_map;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import com.anoop.ds.map.DefaultEntry;
import com.anoop.ds.map.Map;

public class ListMap<K,V> implements Map<K,V> {
	
	private final List<DefaultEntry<K, V>> entries = new LinkedList<>();
	
	@Override
	public Iterator<DefaultEntry<K, V>> iterator() {
		return entries.iterator();
	}

	@Override
	public V get(K key) {
		DefaultEntry<K, V> entry = entryFor(key);
		return entry != null ? entry.getValue() : null;
	}

	@Override
	public V set(K key, V value) {
		DefaultEntry<K, V> entry = entryFor(key);
		if(entry != null) {
			return entry.setValue(value);
		}
		entries.add(new DefaultEntry<>(key, value));
		return null;
	}

	@Override
	public V delete(K key) {
		DefaultEntry<K, V> entry = entryFor(key);
		if(entry == null) {
			return null;
		}
		entries.remove(entry);
		return entry.getValue();
	}

	@Override
	public boolean contains(K key) {
		return entryFor(key) != null;
	}

	@Override
	public void clear() {
		entries.clear();
	}

	@Override
	public int size() {
		return entries.size();
	}

	@Override
	public boolean isEmpty() {
		return false;
	}
	
	private DefaultEntry<K, V> entryFor(K key) {
		for(DefaultEntry<K, V> entry : entries) {
			if(entry.getKey().equals(key)) {
				return entry;
			}
		}
		return null;
	}

}
