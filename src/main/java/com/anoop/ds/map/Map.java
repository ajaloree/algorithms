package com.anoop.ds.map;

public interface Map<K, V> extends Iterable<DefaultEntry<K, V>> {
	
	public V get(K key);
	
	public V set(K key, V value); 
	
	public V delete(K key);
	
	public boolean contains(K key);
	
	public void clear();
	
	public int size();
	
	public boolean isEmpty();
	
	public static interface Entry<K,V> { 
		public K getKey(); 
		public V getValue();
	}
}