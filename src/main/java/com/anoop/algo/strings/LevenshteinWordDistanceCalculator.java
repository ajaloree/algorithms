package com.anoop.algo.strings;

import java.util.stream.Stream;

public class LevenshteinWordDistanceCalculator {
	
	private final int costOfSubstitution; 
	private final int costOfDeletion; 
	private final int costOfInsertion;
	
	public LevenshteinWordDistanceCalculator(int costOfSubstitution, int costOfDeletion, int costOfInsertion) {
		this.costOfSubstitution = costOfSubstitution;
		this.costOfDeletion = costOfDeletion;
		this.costOfInsertion = costOfInsertion;
	}
	
	public LevenshteinWordDistanceCalculator() {
		this(1,1,1);
	}
	
	/**
	 *     M I S T A K E
	 *   0 1 2 3 4 5 6 7
	 * M 1
	 * S 2
	 * T 3
	 * E 4
	 * A 5
	 * K 6
	 * 
	 * @param source - MSTEAK
	 * @param target - MISTAKE
	 * @return Levenshtein distance between source and target strings.
	 */
	public int calculate(String source, String target) {
		
		int sourceLength = source.length(); //6
		int targetLength = target.length(); //7
		
		int[][] grid = new int[sourceLength+1][targetLength+1];
		grid[0][0] = 0;
		
		for(int row = 1; row <= sourceLength; row++) {
			grid[row][0] = row;
		}

		for(int col = 1; col <= targetLength; col++) {
			grid[0][col] = col;
		}
		
		for(int row = 1; row <= sourceLength; row++) {
			for(int col = 1; col <= targetLength; col++) {
				grid[row][col] = calculateCellCost(source, target, grid, row, col);
			}
		}
		
		return grid[sourceLength][targetLength];
	}
	
	/**
	 * Cell cost = minimum(left diagonal + substitution cost, above + delete cost, left + insert cost)
	 * The cost for insertion and deletion is always one, but the cost for substitution is only one when 
	 * the source and target characters don’t match.
	 */
	private int calculateCellCost(String str1, String str2, int[][] grid, int row, int col) {
		return  Stream.of(substitutionCost(str1, str2, grid, row, col),
		          		  deleteCost(grid, row, col),
		          		  insertCost(grid, row, col))
					  .min(Integer::compare)
					  .get();
	}
	
	//Total of cell which is at immediate left diagonal and substitution cost which is only added if source 
	//and target characters dont match.
	private int substitutionCost(String str1, String str2, int[][] grid, int row, int col) {
		
		int subCost = grid[row-1][col-1];
		if(str1.charAt(row-1) != str2.charAt(col-1)) {
			subCost += costOfSubstitution;
		}
		return subCost;
	}

	//Total of cell which is above the current cell and fixed deletion cost specified in constructor (or default of 1).
	private int deleteCost(int[][] grid, int row, int col) {
		return grid[row-1][col] + costOfDeletion;
	}
	
	//Total of left cell value and fixed insert cost specified in constructor (or default of 1).
	private int insertCost(int[][] grid, int row, int col) {
		return grid[row][col-1] + costOfInsertion;
	}
	
}
