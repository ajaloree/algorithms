package com.anoop.algo.strings.search;

public class BoyerMooreStringSearch {
	
	private int[] right;
	private String pat;
	
	public BoyerMooreStringSearch(String pat) {
		//Compute skip table
		this.pat = pat;
		int M = pat.length();
		int R = 256;
		right = new int[R];
		//Initialise skip table with -1
		for(int c = 0; c < R; c++) {
			right[c] = -1;
		}
		//Store right-most position of characters in the pattern within skip list
		for(int j = 0; j < M; j++) {
			right[pat.charAt(j)] = j;
		}
	}
	
	public int search(String txt) {
		//Search for pattern in the txt
		int N = txt.length();
		int M = pat.length();
		int skip;
		for (int i = 0; i <= N-M; i += skip) {
			skip = 0;
			for(int j = M-1; j >= 0; j--) {
				if(pat.charAt(j) != txt.charAt(i+j)) {
					skip = j - right[txt.charAt(i+j)];
			        if (skip < 1) skip = 1;
			        break;
				}
			}
			if(skip == 0) return i;
		}
		return N;
	}
}