package com.anoop.algo.strings.search;

public class BruteForceStringSearch {
	
	// Brute-force substring search requires ~NM character compares to search 
	// for a pattern of length M in a text of length N, in the worst case.
	public int search(String pat, String txt) {
		
		int M = pat.length();
		int N = txt.length();
		
		for(int i = 0; i <= N-M; i++) {
			
			int j;
			for(j = 0; j < M; j++) {
				if(txt.charAt(i+j) != pat.charAt(j)) break;
			}
			if(j == M) return i;
		}
		
		return N;
	}
	
}
