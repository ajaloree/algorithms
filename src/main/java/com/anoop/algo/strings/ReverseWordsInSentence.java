package com.anoop.algo.strings;

public class ReverseWordsInSentence {

	public String reverseWordsUsingBuffer(String str) {
		
		//Make a buffer to hold sentence with words reversed
		char[] buffer = new char[str.length()];
		
		//Place a pointer at the end of the buffer
		int M = buffer.length;
		
		//Temp string variable to hold a word
		String word = "";
		
		for(int i = 0; i < str.length(); i++) {

			char c = str.charAt(i);
			
			if(c == ' ') {
				addFromRight(word, buffer, M);
				buffer[--M] = ' ';
				word = "";
			}  else {
				word+=c;
				M--;
			}
		}
		//Add last word in the buffer
		addFromRight(word, buffer, M);
		
		 return String.valueOf(buffer);
	}
	
	private void addFromRight(String word, char[] buffer, int M) {

		int L = word.length();
		int K = 0;
		for(int i = M; i < M+L; i++) {
			buffer[i] = word.charAt(K++);
		}
	}
}
