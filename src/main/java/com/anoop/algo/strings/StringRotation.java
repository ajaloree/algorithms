package com.anoop.algo.strings;

/**
 * Assume you have a method isSubString() which checks if one word is a substring of another.
 * Given two strings s1 ad s2, write code to check if s2 is a rotation of s1 using only one 
 * call to isSubString (eg 'waterbottle' is a rotation of 'erbottlewat'.
 * @author ajaloree
 *
 */
public class StringRotation {

	/**
	 * Let s1 = 'abcdef'
	 * and s2 = 'defabc'
	 * If s2 is a rotation of s1, then it will definitely be a substring of s1s1
	 */
	public boolean isRotation(String s1, String s2) {
		
		if(s1.length() == s2.length()) {
			return (s1+s1).contains(s2);
		} else {
			return false;
		}
	}
	
}
