package com.anoop.algo.strings;

import java.util.ArrayList;
import java.util.List;

// https://introcs.cs.princeton.edu/java/23recursion/Permutations.java.html
// https://introcs.cs.princeton.edu/java/23recursion/PermutationsK.java.html
// https://introcs.cs.princeton.edu/java/23recursion/Combinations.java.html
// https://introcs.cs.princeton.edu/java/23recursion/CombinationsK.java.html

public class StringPermutations {
	
	public static List<String> permutations(String s) { 
		List<String> collector = new ArrayList<>();
		permutations(collector, "", s); 
		return collector;
	}
    
	private static void permutations(List<String> collector, String prefix, String s) {
        System.out.println("Prefix: "+prefix+", String: "+s);
		int n = s.length();
        if (n == 0) {
        	collector.add(prefix);
        } else {
            for (int i = 0; i < n; i++) {
            	permutations(collector, prefix + s.charAt(i), s.substring(0, i) + s.substring(i+1, n));
            }
        }
    }
	
	public static void main(String[] args) {
		
		System.out.println(permutations("ABCDE"));
		
	}
}
