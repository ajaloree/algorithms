package com.anoop.algo.strings;

public class RemoveSpecificCharacters {
	
	private boolean[] charsToRemoveArr;
	
	public RemoveSpecificCharacters(String charsToRemove) {
		this.charsToRemoveArr = new boolean[256];
		for(char c : charsToRemove.toCharArray()) {
			this.charsToRemoveArr[c] = true;
		}
	}

	public StringBuilder removeFrom(StringBuilder str) {
		
		int read, write = 0;
		for(read = 0; read < str.length(); read++) {
			
			if(charsToRemoveArr[str.charAt(read)]) {
				System.out.println("Char to remove: "+str.charAt(read));
			} else {
				/**
				 * Add if condition to write only when there is a difference between read and write pointers.
				 * The condition will avoid unnecessary writes in case there is no difference in the first 
				 * few characters of the string. The algo will still work is the i(read != write) check is 
				 * omitted, it will cause a few unnecessary write operations though.
				 * 
				 */
				if(read != write) { 
					str.setCharAt(write, str.charAt(read));
				}
				write++;
			}
		}
		str.setLength(write);
		return str;
	}

}