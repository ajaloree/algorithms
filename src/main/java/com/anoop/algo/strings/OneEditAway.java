package com.anoop.algo.strings;
/**
 * There are three types of edits that can  be performed on strings:
 * insert a character, remove a character and replace a character.
 * Given two strings, write a function to check if they are one edit away.
 * Example:
 * pale, ple -> true
 * pales, ales -> true
 * pales, pale -> true
 * pale, bale -> true
 * pale, bake -> false
 * 
 * @author ajaloree
 *
 */
public class OneEditAway {
	
	public boolean checkIfOneAway(String str1, String str2) {
		
		if(str1.length() == str2.length()) {
			return isOneEditReplace(str1, str2);
		} else if(str1.length() == str2.length() + 1) {
			return isOneEditInsert(str1, str2);
		} else if(str2.length() == str1.length() + 1) {
			return isOneEditInsert(str2, str1);
		}
		return false;
	}
	
	//Both str1 and str2 should have same length
	private boolean isOneEditReplace(String str1, String str2) {
		
		int diffCount = 0;
		for(int i = 0; i < str1.length(); i++) {
			if(str1.charAt(i) != str2.charAt(i)) {
				diffCount++;
			}
			if(diffCount > 1) return false;
		}
		return true;
	}

	//str1 is one character longer than str2
	private boolean isOneEditInsert(String str1, String str2) {
		
		//Consider two cases mentioned below:
		//Assume str1 = pales and str2 = ales
		//Assume str1 = abcde and str2 = abde
		
		int diffCount = 0, j = 0;
		for(int i = 0; i < str2.length(); i++) {

			if(str1.charAt(i) != str2.charAt(j)) {
				diffCount++;
			} else {
				j++;
			}
			if(diffCount > 1) return false;
		}
		return true;
	}

}
