package com.anoop.algo.sorting;

import static com.anoop.algo.sorting.ArrayUtils.*;

/**
 * 
 * The algorithm divides the input list into two parts: the sublist of items already sorted,
 * which is built up from left to right at the front (left) of the list, and the sublist of 
 * items remaining to be sorted that occupy the rest of the list.
 * 
 * @author ajaloree
 *
 */
public class SelectionSort {

	private static void sort(int[] arr, int startIdx) {
		int minIdx = startIdx;
		for(int i = startIdx; i < arr.length; i++) {
			if(arr[i] < arr[minIdx]) {
				minIdx = i;
			}
		}
		swap(arr, startIdx, minIdx);
	}
	
	public static int[] sort(int[] arr) {
		printArray("Before: ", arr);
		for(int i=0; i < arr.length; i++) {
			sort(arr, i);
		}
		printArray("After: ", arr);
		return arr;
	}
	
	
	public static void main(String[] args) {
		printArray("Selection Sort: ", sort(new int[] {5,4,3,2,1,0,1,2,3,4,5}));
	}
}
