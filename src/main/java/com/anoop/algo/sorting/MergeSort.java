package com.anoop.algo.sorting;

import static com.anoop.algo.sorting.ArrayUtils.*;

/**
 * 
 * Merge sort is a divide-and-conquer algorithm that works by splitting a data set
 * into two or more subsets, sorting the subsets, and then merging them together into
 * the final set. Merge sort requires additional storage to the sort the auxiliary array.
 * 
 * Best, average and worst runtime cost: O(n log(n))
 * Storage cost: O(n)
 * 
 * @author ajaloree
 *
 */
public class MergeSort {

	public static void sort(int[] data) {
		//Base case
		if(data.length < 2) return;
		//Create two arrays to hold the two halves of the main array
		int mid = data.length / 2;
		int[] left = new int[mid];
		int[] right = new int[data.length - mid];
		//Copy the two halves of the main array in the two arrays created above
		System.arraycopy(data, 0, left, 0, left.length);
		System.arraycopy(data, mid, right, 0, right.length);
		//Sort the two arrays - ReCuRsIoN !
		sort(left);
		sort(right);
		//Merge the two arrays again
		merge(data, left, right);
	}

	private static void merge(int[] data, int[] left, int[] right) {
		
		int dIdx = 0, leftIdx = 0, rightIdx = 0;
		//Merge arrays while there are elements in both
		while(leftIdx < left.length && rightIdx < right.length) {
			if(left[leftIdx] <= right[rightIdx]) {
				data[dIdx++] = left[leftIdx++];
			} else {
				data[dIdx++] = right[rightIdx++];
			}
		}
		//Copy rest of whichever array is non-empty
		while(leftIdx < left.length) {
			data[dIdx++] = left[leftIdx++];
		}
		while(rightIdx < right.length) {
			data[dIdx++] = right[rightIdx++];
		}
	}
	
	public static void main(String[] args) {
		
		int[] arr = new int[] {5,4,3,2,1,0,1,2,3,4,5};
		printArray("Before: ", arr);
		sort(arr);
		printArray("After: ", arr);
	}
}
