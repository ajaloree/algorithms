package com.anoop.algo.sorting;

import java.util.Arrays;

public class ArrayUtils {

	public static void printArray(String label, int[] arr) {
		System.out.println(String.format("%s: %s", label, Arrays.toString(arr)));
	}
	
	public static void swap(int[] arr, int idx1, int idx2) {
		int temp = arr[idx1];
		arr[idx1] = arr[idx2];
		arr[idx2] = temp;
	}
}
