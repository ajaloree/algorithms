package com.anoop.algo.sorting;

import static com.anoop.algo.sorting.ArrayUtils.*;

/**
 * 
 * Builds a sorted array / list one element at a time by comparing each element to
 * the already sorted elements and inserting the new element into the correct location.
 * 
 * @author ajaloree
 *
 */
public class InsertionSort {

	public static int[] sort(int[] arr) {
		printArray("Before: ", arr);
		for(int i = 0; i < arr.length; i++) {
			for(int j = i+1; j < arr.length; j++) {
				if(arr[j] < arr[i]) {
					swap(arr, i, j);
				}
			}
		}
		printArray("After: ", arr);
		return arr;
	}
	
	public static void main(String[] args) {
		printArray("Insertion Sort: ", sort(new int[] {5,4,3,2,1,0,1,2,3,4,5}));
	}
}
