package com.anoop.algo.kmeans;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

/**
 * K-means cluster- ing algorithm:
 * 1] Initialize all of the data points and “k” empty clusters.
 * 2] Normalize all of the data points.
 * 3] Create random centroids associated with each cluster.
 * 4] Assign each data point to the cluster of the centroid it is closest to.
 * 5] Recalculate each centroid so it is the center (mean) of the cluster it is associated with.
 * 6] Repeat steps 4 and 5 until a maximum number of iterations is reached or the centroids stop moving (convergence).
 * 
 * @author ajaloree
 */
public class KMeans<Point extends DataPoint> {

	public class Cluster {
		
		public List<Point> points;
		public DataPoint centroid;
		
		public Cluster(List<Point> points, DataPoint randPoint) {
			this.points = points;
			this.centroid = randPoint;
		}
		
		@Override
			public String toString() {
				return "Cluster size "+points.size()+" with centroid at "+centroid+" and elements "+points;
			}
		
	}
	
	private List<Point> points; // all of the points in the data set
	private List<Cluster> clusters;
	
	public KMeans(int k, List<Point> points) {
		if(k < 1) { // can't have negative or zero clusters
			throw new IllegalArgumentException("k musy be >= 1");
		}
		this.points = points;
		zScoreNormalize();
		// initialize empty clusters with random centroids
		clusters = new ArrayList<>();
		for(int i = 0; i < k; i++) {
			DataPoint randPoint = randomPoint();
			Cluster cluster = new Cluster(new ArrayList<>(), randPoint);
			clusters.add(cluster);
		}
	}
	
	private List<DataPoint> centroids() {
		return clusters.stream().map(cluster -> cluster.centroid).collect(Collectors.toList());
	}
	
	/**
	 * A convenience method that can be thought of as returning a column of data. 
	 * It will return a list composed of every value at a particular index in every 
	 * data point. For instance, if the data points were of type DataPoint, then 
	 * dimensionSlice(0) would return a list of the value of the first dimension 
	 * of every data point.
	 */
	private List<Double> dimensionSlice(int dimension) {
		return points.stream()
					 .map(x -> x.dimensions.get(dimension))
					 .collect(Collectors.toList());
	}
	
	/**
	 * zScoreNormalize() replaces the values in the dimensions list of every data point with 
	 * its z-scored equivalent. This uses the zscored() function that we defined for lists of 
	 * double earlier. Although the values in the dimensions list are replaced, the originals 
	 * list in the DataPoint are not. This is useful; the user of the algorithm can still 
	 * retrieve the original values of the dimensions before normalization after the algorithm 
	 * runs if they are stored in both places.
	 */
	private void zScoreNormalize() {
		List<List<Double>> zscored = new ArrayList<>();
		for(Point point : points) {
			zscored.add(new ArrayList<>());
		}
		for(int dimension = 0; dimension < points.get(0).numDimensions; dimension++) {
			List<Double> dimensionSlice = dimensionSlice(dimension);
			Statistics stats = new Statistics(dimensionSlice);
			List<Double> zscores = stats.zscored();
			for(int index = 0; index < zscores.size(); index++) {
				zscored.get(index).add(zscores.get(index));
			}
		}
		for(int i = 0; i < points.size(); i++) {
			points.get(i).dimensions = zscored.get(i);
		}
	}
	
	/**
	 * Method used in the constructor to create the initial random centroids for each cluster. 
	 * It constrains the random values of each point to be within the range of the existing data 
	 * points’ values. It uses the constructor we specified earlier on DataPoint to create a new 
	 * point from a list of values.
	 */
	private DataPoint randomPoint() {
		List<Double> randDimensions = new ArrayList<>();
		Random random = new Random();
		for(int dimension = 0; dimension < points.get(0).numDimensions; dimension++) {
			List<Double> values = dimensionSlice(dimension);
			Statistics stats = new Statistics(values);
			Double randValue = random.doubles(stats.min(), stats.max()).findFirst().getAsDouble();
			randDimensions.add(randValue);
		}
		return new DataPoint(randDimensions);
	}
	
	/**
	 * Find the closest cluster centroid to each point and assign the point to that cluster.
	 */
	private void assignClusters() {
		for(Point point : points) {
			double lowestDistance = Double.MAX_VALUE;
			Cluster closestCluster = clusters.get(0);
			for(Cluster cluster : clusters) {
				double centroidDistance = point.distance(cluster.centroid);
				if(centroidDistance < lowestDistance) {
					lowestDistance = centroidDistance;
					closestCluster = cluster;
				}
			}
			closestCluster.points.add(point);
		}
	}
	
	/**
	 *  Find the center of each cluster and move the centroid to there
	 */
	private void generateCentroids() {
		for(Cluster cluster : clusters) {
			// ignore if the cluster is empty
			if(cluster.points.isEmpty()) {
				continue;
			}
			List<Double> means = new ArrayList<>();
			for(int i = 0; i < cluster.points.get(0).numDimensions; i++) {
				//needed to use in scope of closure
				int dimension = i;
				Double dimensionMean = cluster.points.stream()
												     .mapToDouble(x -> x.dimensions.get(dimension))
												     .average()
												     .getAsDouble();
				means.add(dimensionMean);
			}
			cluster.centroid = new DataPoint(means);
		}
	}
	
	/**
	 * Check if two Lists of DataPoints are of equivalent DataPoints
	 */
	private boolean listsEqual(List<DataPoint> first, List<DataPoint> second) {
		if (first.size() != second.size()) {
			return false; 
		}
		for (int i = 0; i < first.size(); i++) {
			for (int j = 0; j < first.get(0).numDimensions; j++) {
				if (first.get(i).dimensions.get(j).doubleValue() != second.get(i).dimensions.get(j).doubleValue()) {
					return false; 
				}
			} 
		}
		return true; 
	}
	
	public List<Cluster> run(int maxIterations) {
		for (int iteration = 0; iteration < maxIterations; iteration++) {
			for (Cluster cluster : clusters) { 
				// clear all clusters 
				cluster.points.clear();
			}
			assignClusters();
			List<DataPoint> oldCentroids = new ArrayList<>(centroids());
			generateCentroids(); // find new centroids
			if (listsEqual(oldCentroids, centroids())) {
				System.out.println("Converged after " + iteration + " iterations.");
				return clusters;
			}
		}
		return clusters;
	}
}
