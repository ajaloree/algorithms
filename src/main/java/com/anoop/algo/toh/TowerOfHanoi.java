package com.anoop.algo.toh;

import java.util.stream.Stream;

public class TowerOfHanoi {
	
	
	public static void hanoi(Tower begin, Tower end, Tower temp, Integer n) {
		if(n == 1) {
			end.push(begin.pop());
		} else {
			hanoi(begin, temp, end, n-1);
			hanoi(begin, end, temp, 1);
			hanoi(temp, end, begin, n-1);
		}
	}
	
	
	public static void main(String[] args) {
		Tower begin = new Tower("Tower-Begin");
		Tower end = new Tower("Tower-End");
		Tower temp = new Tower("Tower-Temp");
		
		int SIZE = 3;
		Stream.iterate(SIZE, x->x-1).limit(SIZE).forEach(v -> begin.push(v));
		
		System.out.println("Tower-Begin: "+begin);
		System.out.println("Tower-End: "+end);
		System.out.println("Tower-Temp: "+temp);

		hanoi(begin, end, temp, SIZE);
		
		System.out.println("Tower-Begin: "+begin);
		System.out.println("Tower-End: "+end);
		System.out.println("Tower-Temp: "+temp);
		

	}

}
