package com.anoop.algo.toh;

import java.util.Stack;

public class Tower {
	
	private Stack<Integer> stack;
	private String name;
	
	public Tower(String name) {
		this.stack = new Stack<>();
		this.name = name;
	}
	
	public void push(Integer val) {
		if(!stack.isEmpty() && (stack.peek() < val)) {
			throw new UnsupportedOperationException("Cany push bigger disc on a smaller disc!");
		}
		System.out.println("Tower: "+name+", Pushing "+val);
		stack.push(val);
	}
	
	public Integer pop() {
		Integer val = stack.pop();
		System.out.println("Tower: "+name+", Popping "+val);
		return val;
	}
	
	@Override
	public String toString() {
		return stack.toString();
	}
}