package com.anoop.algo.greedy;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ChangeDispenser {

	private final List<Integer> denominations;
	
	public ChangeDispenser(List<Integer> denominations) {
		this.denominations = new ArrayList<>(denominations);
		Collections.sort(this.denominations, Collections.reverseOrder());
	}
	
	public List<Integer> dispenseChange(Integer amount) {
		
		List<Integer> change = new ArrayList<>();
		
		while(amount > 0) {
			
			for(Integer coin : denominations) {
				
				if(coin > amount) {
					continue;
				} else {
					amount -= coin;
					change.add(coin);
					break;
				}
			}
			
		}
		return change;
		
	}

}